/**
    bwtb3m
    Copyright (C) 2009-2020 German Tischler-Höhle
    Copyright (C) 2011-2014 Genome Research Limited

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
#include <libmaus2/huffman/RLDecoder.hpp>
#include <libmaus2/util/ArgInfo.hpp>
#include <libmaus2/util/utf8.hpp>
#include <libmaus2/util/NumberMapSerialisation.hpp>
#include <libmaus2/util/OutputFileNameTools.hpp>
#include <libmaus2/util/TempFileRemovalContainer.hpp>
#include <libmaus2/huffman/HuffmanTree.hpp>
#include <libmaus2/rank/RunLengthBitVectorGenerator.hpp>
#include <libmaus2/rank/RunLengthBitVector.hpp>
#include <libmaus2/wavelet/ImpCompactHuffmanWaveletTree.hpp>
#include <libmaus2/timing/RealTimeClock.hpp>
#include <libmaus2/aio/InputOutputStreamInstance.hpp>
#include <libmaus2/parallel/NumCpus.hpp>
#include <libmaus2/math/IntegerInterval.hpp>
#include <iostream>
#include <libmaus2/rank/RunLengthBitVectorGenerator.hpp>

void bwtToRlHwtPar(::libmaus2::util::ArgInfo const & arginfo)
{
	uint64_t const rlblocksize = arginfo.getValueUnsignedNumeric<uint64_t>("blocksize",64*1024);
	bool const recreate = arginfo.getValue<unsigned int>("recreate",true);
	std::string const tmpfilename = arginfo.getUnparsedValue("T",arginfo.getDefaultTmpFileName());

	std::string const infn = arginfo.getRestArg<std::string>(0);
        std::string const rlhwtname = libmaus2::util::OutputFileNameTools::clipOff(infn,".bwt") + ".rlhwt";
	bool const verify = arginfo.getValue<unsigned int>("verify",false);
	bool const deepverify = arginfo.getValue<unsigned int>("deepverify",false);
	uint64_t threads = arginfo.getValueUnsignedNumeric<uint64_t>("threads",1);
	if ( threads == 0 )
		threads = libmaus2::parallel::NumCpus::getNumLogicalProcessors();

	std::string const histfn = libmaus2::util::OutputFileNameTools::clipOff(infn,".bwt") + ".hist";

	libmaus2::aio::InputStreamInstance histstr(histfn);
	std::map<int64_t,uint64_t> const hist = ::libmaus2::util::NumberMapSerialisation::deserialiseMap<std::istream,int64_t,uint64_t>(histstr);
	std::vector<uint64_t> syms;
	std::vector<std::pair<int64_t,uint64_t> > symfreq;
	uint64_t n = 0;

	for ( std::map<int64_t,uint64_t>::const_iterator ita = hist.begin();
		ita != hist.end(); ++ita )
	{
		symfreq.push_back(std::make_pair(ita->first,ita->second));
		std::cerr << "[V] H[" << ita->first << "]=" << ita->second << std::endl;
		syms.push_back(ita->first);
		n += ita->second;
	}

	libmaus2::huffman::HuffmanTree H(symfreq.begin(),symfreq.size(),false,true);

	std::cerr << H.toString();

	libmaus2::huffman::HuffmanTree::EncodeTable E(H);

	for ( uint64_t i = 0; i < symfreq.size(); ++i )
	{
		int64_t const sym = symfreq[i].first;
		unsigned int const codelen = E.getCodeLength(sym);
		std::cerr << "[V] C[" << sym << "]="; // << E.printCode(sym) << std::endl;

		for ( unsigned int j = 0; j < codelen; ++j )
		{
			bool const b = E.getBitFromTop(sym,j);
			std::cerr << b;
		}
		std::cerr << std::endl;
	}

	std::cerr << "Number of inner nodes " << H.inner() << " root " << H.root() << std::endl;

	std::vector<uint64_t> nodebitcnts(H.inner());

	for ( uint64_t i = 0; i < syms.size(); ++i )
	{
		uint64_t const s = syms[i];
		assert ( hist.find(s) != hist.end() );
		uint64_t const f = hist.find(s)->second;
		unsigned int const codelen = E.getCodeLength(s);
		uint64_t node = H.root();

		for ( unsigned int j = 0; j < codelen; ++j )
		{
			uint64_t const nodeid = node - H.leafs();
			nodebitcnts.at(nodeid) += f;

			bool const b = E.getBitFromTop(s,j);

			assert ( !H.isLeaf(node) );

			if ( b )
				node = H.rightChild(node);
			else
				node = H.leftChild(node);
		}

		assert ( H.isLeaf(node) );
	}

	for ( uint64_t i = 0; i < nodebitcnts.size(); ++i )
		std::cerr << "[V] nodebitcnts[" << i << "]=" << nodebitcnts[i] << std::endl;

        if ( (! libmaus2::util::GetFileSize::fileExists(rlhwtname)) || recreate )
        {
        	// target number of packages
		uint64_t const tpacks = 4 * threads;
		// package size
		uint64_t const packsize = tpacks ? ((n + tpacks - 1) / tpacks) : 0;
		// actual number of packages
		uint64_t const npacks = packsize ? ((n + packsize - 1)) / packsize : 0;

		// count number of bits in each node and each package
		std::vector< std::vector<uint64_t> > Vnodecnt(H.inner(),std::vector<uint64_t>(npacks+1,0));

		std::cerr << "[V] computing node bit counts per package" << std::endl;
		std::atomic<uint64_t> nodecntcomplete(0);

		std::atomic<int> parfailednodebitcounts(0);

		#if defined(_OPENMP)
		#pragma omp parallel for num_threads(threads) schedule(dynamic,1)
		#endif
		for ( uint64_t t = 0; t < npacks; ++t )
		{
			try
			{
				uint64_t const low = t * packsize;
				uint64_t const high = std::min(low+packsize,n);

				libmaus2::huffman::RLDecoder dec(std::vector<std::string>(1,infn),low /* offset */,1 /* numthreads */);
				std::vector<uint64_t> VN(H.inner(),0);

				for ( uint64_t i = low; i < high; ++i )
				{
					int64_t const sym = dec.decode();
					assert ( sym >= 0 );
					unsigned int const codelen = E.getCodeLength(sym);

					uint64_t node = H.root();
					for ( unsigned int i = 0; i < codelen; ++i )
					{
						assert ( ! H.isLeaf(node) );
						uint64_t const nodeid = node - H.leafs();
						bool const b = E.getBitFromTop(sym,i);

						++VN[nodeid];

						if ( b )
							node = H.rightChild(node);
						else
							node = H.leftChild(node);
					}

					assert ( H.isLeaf(node) );
				}

				for ( uint64_t i = 0; i < H.inner(); ++i )
					Vnodecnt[i][t] = VN[i];

				uint64_t const lnodecntcomplete = ++nodecntcomplete;
				libmaus2::aio::StreamLock::lock_type::scope_lock_type slock(libmaus2::aio::StreamLock::cerrlock);
				std::cerr << "[V] " << lnodecntcomplete << "/" << npacks << std::endl;
			}
			catch(std::exception const & ex)
			{
				libmaus2::aio::StreamLock::lock_type::scope_lock_type slock(libmaus2::aio::StreamLock::cerrlock);
				std::cerr << ex.what() << std::endl;
				parfailednodebitcounts = 1;

			}
		}

		if ( parfailednodebitcounts.load() )
		{
			libmaus2::exception::LibMausException lme;
			lme.getStream() << "[E] parallel loop computing node counts failed" << std::endl;
			lme.finish();
			throw lme;
		}

		// compute prefix sums for each node
		for ( uint64_t i = 0; i < Vnodecnt.size(); ++i )
		{
			std::vector<uint64_t> & V = Vnodecnt[i];
			libmaus2::util::PrefixSums::prefixSums(V.begin(),V.end());
		}

		typedef std::pair<uint64_t,uint64_t> up;
		typedef std::pair<up,up> uup;
		std::vector < std::vector<uup> > VUP(H.inner(),std::vector<uup>(npacks,uup(up(0,0),up(0,0))));
		std::vector < std::vector<uint64_t> > VVocnt(H.inner(),std::vector<uint64_t>(npacks,0));

		std::cerr << "[V] computing node bit processing intervals" << std::endl;
		std::atomic<uint64_t> nodeproccomplete(0);
		std::atomic<int> nodebitprocfailed(0);

		#if defined(_OPENMP)
		#pragma omp parallel for num_threads(threads) schedule(dynamic,1)
		#endif
		for ( uint64_t t = 0; t < npacks; ++t )
		{
			try
			{
				uint64_t const low = t * packsize;
				uint64_t const high = std::min(low+packsize,n);

				libmaus2::huffman::RLDecoder dec(std::vector<std::string>(1,infn),low /* offset */,1 /* numthreads */);
				std::vector<uint64_t> VN(H.inner(),0);

				for ( uint64_t i = 0; i < H.inner(); ++i )
					VN[i] = Vnodecnt[i][t];

				std::vector<bool> VB(H.inner(),false);
				std::vector<uint64_t> Vocnt(H.inner(),0);
				std::vector<uint64_t> Volast(H.inner(),0);
				uint64_t VBset = 0;
				std::vector<std::pair<uint64_t,uint64_t> > Vfirst(H.inner());
				std::vector<std::pair<uint64_t,uint64_t> > Vlast(H.inner());

				uint64_t z = low;
				for ( ; z < high && VBset < H.inner(); ++z )
				{
					int64_t const sym = dec.decode();
					assert ( sym >= 0 );
					unsigned int const codelen = E.getCodeLength(sym);

					uint64_t node = H.root();
					for ( unsigned int i = 0; i < codelen; ++i )
					{
						assert ( ! H.isLeaf(node) );
						uint64_t const nodeid = node - H.leafs();
						bool const b = E.getBitFromTop(sym,i);

						if ( VN[nodeid] % rlblocksize == 0 )
						{
							if ( ! VB[nodeid] )
							{
								VB[nodeid] = true;
								VBset += 1;
								Vfirst[nodeid] = std::make_pair(z,VN[nodeid]);
							}
						}

						if ( VB[nodeid] && b )
							Vocnt[nodeid] += 1;

						++VN[nodeid];

						if ( VN[nodeid] % rlblocksize == 0 )
						{
							Vlast[nodeid] = std::make_pair(z+1,VN[nodeid]);
							Volast[nodeid] = Vocnt[nodeid];
						}

						if ( b )
							node = H.rightChild(node);
						else
							node = H.leftChild(node);
					}

					assert ( H.isLeaf(node) );
				}

				assert ( z == high || VBset == H.inner() );

				for ( ; z < high; ++z )
				{
					int64_t const sym = dec.decode();
					assert ( sym >= 0 );
					unsigned int const codelen = E.getCodeLength(sym);

					uint64_t node = H.root();
					for ( unsigned int i = 0; i < codelen; ++i )
					{
						assert ( ! H.isLeaf(node) );
						uint64_t const nodeid = node - H.leafs();
						bool const b = E.getBitFromTop(sym,i);

						if ( b )
							Vocnt[nodeid] += 1;

						++VN[nodeid];

						if ( VN[nodeid] % rlblocksize == 0 )
						{
							Vlast[nodeid] = std::make_pair(z+1,VN[nodeid]);
							Volast[nodeid] = Vocnt[nodeid];
						}

						if ( b )
							node = H.rightChild(node);
						else
							node = H.leftChild(node);
					}

					assert ( H.isLeaf(node) );
				}

				for ( uint64_t i = 0; i < H.inner(); ++i )
				{
					VUP[i][t] = uup(Vfirst[i],Vlast[i]);
					VVocnt[i][t] = Volast[i];
				}

				uint64_t const lnodeproccomplete = ++nodeproccomplete;
				libmaus2::aio::StreamLock::lock_type::scope_lock_type slock(libmaus2::aio::StreamLock::cerrlock);
				std::cerr << "[V] " << lnodeproccomplete << "/" << npacks << std::endl;
			}
			catch(std::exception const & ex)
			{
				libmaus2::aio::StreamLock::lock_type::scope_lock_type slock(libmaus2::aio::StreamLock::cerrlock);
				std::cerr << ex.what() << std::endl;
				nodebitprocfailed = 1;
			}
		}

		if ( nodebitprocfailed.load() )
		{
			libmaus2::exception::LibMausException lme;
			lme.getStream() << "[E] parallel loop computing node bit processing intervals failed" << std::endl;
			lme.finish();
			throw lme;
		}

		struct OneObject
		{
			uint64_t symfrom;
			uint64_t symto;
			uint64_t bitfrom;
			uint64_t bitto;
			uint64_t ocnt;
			uint64_t osum;

			OneObject()
			{}
			OneObject(
				uint64_t const rsymfrom,
				uint64_t const rsymto,
				uint64_t const rbitfrom,
				uint64_t const rbitto,
				uint64_t const rocnt
			) : symfrom(rsymfrom), symto(rsymto), bitfrom(rbitfrom), bitto(rbitto), ocnt(rocnt), osum(0)
			{

			}

			bool operator<(OneObject const & O) const
			{
				return symfrom < O.symfrom;
			}

			std::string toString() const
			{
				std::ostringstream ostr;
				ostr << "OneObject("
					<< symfrom << ","
					<< symto << ","
					<< bitfrom << ","
					<< bitto << ","
					<< ocnt << ","
					<< osum << ")";
				return ostr.str();
			}
		};

		std::vector< std::vector<OneObject> > VONE(H.inner());

		for ( uint64_t i = 0; i < H.inner(); ++i )
		{
			std::cerr << "node " << i << std::endl;
			for ( uint64_t j = 0; j < VUP[i].size(); ++j )
			{
				up const Vfirst = VUP[i][j].first;
				up const Vlast  = VUP[i][j].second;

				std::cerr << "\tpack " << j
					<< " Vfirst=(" << Vfirst.first << "," << Vfirst.second << ")"
					<< " Vlast=(" << Vlast.first << "," << Vlast.second << ")"
					<< " ocnt=" << VVocnt[i][j]
					<< std::endl;

				if ( Vfirst != Vlast )
				{
					VONE[i].push_back(
						OneObject(
							Vfirst.first,
							Vlast.first,
							Vfirst.second,
							Vlast.second,
							VVocnt[i][j]
						)
					);
				}
			}
		}

		for ( uint64_t y = 0; y < H.inner(); ++y )
		{
			uint64_t nextsym = 0;
			uint64_t nextbit = 0;

			uint64_t const s = VONE[y].size();

			for ( uint64_t j = 0; j <= s; ++j )
			{
				OneObject O;

				if ( j < s )
					O = VONE[y][j];
				else
					O = OneObject(
						n,
						n,
						nodebitcnts[y],
						nodebitcnts[y],
						0
					);

				if ( nextsym < O.symfrom )
				{
					uint64_t const symfrom = nextsym;
					uint64_t const symto   = O.symfrom;
					uint64_t const bitfrom = nextbit;
					libmaus2::huffman::RLDecoder dec(std::vector<std::string>(1,infn),symfrom /* offset */,1 /* numthreads */);

					uint64_t nb = 0;
					uint64_t no = 0;

					for ( uint64_t z = symfrom; z < symto; ++z )
					{
						int64_t const sym = dec.decode();
						assert ( sym >= 0 );
						unsigned int const codelen = E.getCodeLength(sym);

						uint64_t node = H.root();
						for ( unsigned int i = 0; i < codelen; ++i )
						{
							assert ( ! H.isLeaf(node) );
							uint64_t const nodeid = node - H.leafs();
							bool const b = E.getBitFromTop(sym,i);

							if ( nodeid == y )
							{
								nb++;
								if ( b )
									no++;
							}

							if ( b )
								node = H.rightChild(node);
							else
								node = H.leftChild(node);
						}

						assert ( H.isLeaf(node) );
					}

					VONE[y].push_back(OneObject(symfrom,symto,bitfrom,bitfrom+nb,no));
				}

				nextsym = O.symto;
				nextbit = O.bitto;
			}

			std::sort(VONE[y].begin(),VONE[y].end());
		}

		for ( uint64_t y = 0; y < VONE.size(); ++y )
		{
			uint64_t osum = 0;
			for ( uint64_t i = 0; i < VONE[y].size(); ++i )
			{
				OneObject & O = VONE[y][i];
				assert ( O.symfrom < O.symto );
				assert ( i == 0 || VONE[y][i-1].symto == VONE[y][i].symfrom );
				assert ( i != 0 || VONE[y][i].symfrom == 0 );
				assert ( i+1 != VONE[y].size() || VONE[y][i].symto == n );
				O.osum = osum;

				std::cerr << "VONE[" << y << "][" << i << "]=" << O.toString() << std::endl;

				osum += O.ocnt;
			}
		}

		std::vector< std::vector<std::string> > VVTMP(npacks);
		std::vector< std::vector<std::string> > VVTMPINDEX(npacks);
		std::mutex VVTMPlock;

		std::cerr << "[V] writing node packages" << std::endl;

		std::atomic<uint64_t> nodewritecomplete(0);
		std::atomic<int> nodewritefailed(0);

		#if defined(_OPENMP)
		#pragma omp parallel for num_threads(threads) schedule(dynamic,1)
		#endif
		for ( uint64_t t = 0; t < npacks; ++t )
		{
			try
			{
				std::vector<uup> VL;
				std::vector<uint64_t> VN(H.inner());
				std::vector< libmaus2::math::IntegerInterval<int64_t> > VI(H.inner());
				libmaus2::math::IntegerInterval<int64_t> IC(0,-1);
				std::vector<std::string> VTMP(H.inner());
				std::vector<std::string> VTMPINDEX(H.inner());
				std::vector<libmaus2::aio::OutputStreamInstance::shared_ptr_type> VOSI(H.inner());
				std::vector<libmaus2::rank::RunLengthBitVectorGenerator::shared_ptr_type> VRL(H.inner());

				for ( uint64_t i = 0; i < H.inner(); ++i )
				{
					uup const & U = VUP[i][t];
					up const Vfirst = U.first;
					up const Vlast  = U.second;
					VL.push_back(U);

					int64_t const symfrom = Vfirst.first;
					int64_t const symto   = Vlast.first;


					VI.push_back(libmaus2::math::IntegerInterval<int64_t>(symfrom,symto-1));

					if ( i == 0 )
						IC = VI.back();
					else
						IC = IC.intersection(VI.back());

					if ( symto != symfrom )
					{
						std::ostringstream tmpfnostr;
						tmpfnostr << tmpfilename << "_" << std::setw(6) << std::setfill('0') << t << std::setw(0) << "_" << std::setw(6) << i;
						std::string const tmpfn = tmpfnostr.str();
						VTMP[i] = tmpfn;
						VTMPINDEX[i] = tmpfn + ".index";

						libmaus2::aio::OutputStreamInstance::shared_ptr_type sptr(new libmaus2::aio::OutputStreamInstance(VTMP[i]));
						VOSI[i] = sptr;

						std::vector<OneObject> const & LONE = VONE[i];
						std::vector<OneObject>::const_iterator oit =
							std::lower_bound(
								LONE.begin(),
								LONE.end(),
								OneObject(symfrom,symto,0,0,0)
							);
						assert ( oit != LONE.end() && oit->symfrom == static_cast<uint64_t>(symfrom) && oit->symto == static_cast<uint64_t>(symto) );

						libmaus2::rank::RunLengthBitVectorGenerator::shared_ptr_type rptr(
							new libmaus2::rank::RunLengthBitVectorGenerator(
								*VOSI[i],
								VTMPINDEX[i],
								Vlast.second - Vfirst.second,
								rlblocksize,
								false, /* put header */
								oit->osum
							)
						);
						VRL[i] = rptr;
						// std::cerr << tmpfn << std::endl;
					}
				}

				{
					std::lock_guard<std::mutex> slock(VVTMPlock);
					VVTMP[t] = VTMP;
					VVTMPINDEX[t] = VTMPINDEX;
				}

				if ( IC.isEmpty() )
				{
					for ( uint64_t y = 0; y < H.inner(); ++y )
						if ( VL[y].first != VL[y].second )
						{
							uint64_t const symfrom = VL[y].first.first;
							uint64_t const symto   = VL[y].second.first;
							libmaus2::huffman::RLDecoder dec(std::vector<std::string>(1,infn),symfrom /* offset */,1 /* numthreads */);
							uint64_t nb = 0;
							uint64_t no = 0;

							for ( uint64_t z = symfrom; z < symto; ++z )
							{
								int64_t const sym = dec.decode();
								assert ( sym >= 0 );
								unsigned int const codelen = E.getCodeLength(sym);

								uint64_t node = H.root();
								for ( unsigned int i = 0; i < codelen; ++i )
								{
									assert ( ! H.isLeaf(node) );
									uint64_t const nodeid = node - H.leafs();
									bool const b = E.getBitFromTop(sym,i);

									if ( nodeid == y )
									{
										VRL[nodeid]->putbit(b);
										nb += 1;
										if ( b )
											no += 1;
									}

									if ( b )
										node = H.rightChild(node);
									else
										node = H.leftChild(node);
								}

								assert ( H.isLeaf(node) );
							}

							assert ( nb == VL[y].second.second - VL[y].first.second );

							std::vector<OneObject> const & LONE = VONE[y];
							std::vector<OneObject>::const_iterator oit =
								std::lower_bound(
									LONE.begin(),
									LONE.end(),
									OneObject(symfrom,symto,0,0,0)
								);
							assert ( oit != LONE.end() && oit->symfrom == static_cast<uint64_t>(symfrom) && oit->symto == static_cast<uint64_t>(symto) );
							assert ( no == oit->ocnt );
						}
				}
				else
				{
					std::vector<uint64_t> VNB(H.inner(),0);
					std::vector<uint64_t> VNO(H.inner(),0);

					for ( uint64_t y = 0; y < H.inner(); ++y )
						if ( VL[y].first != VL[y].second )
						{
							uint64_t const symfrom = VL[y].first.first;
							uint64_t const symto   = IC.from;
							libmaus2::huffman::RLDecoder dec(std::vector<std::string>(1,infn),symfrom /* offset */,1 /* numthreads */);

							for ( uint64_t z = symfrom; z < symto; ++z )
							{
								int64_t const sym = dec.decode();
								assert ( sym >= 0 );
								unsigned int const codelen = E.getCodeLength(sym);

								uint64_t node = H.root();
								for ( unsigned int i = 0; i < codelen; ++i )
								{
									assert ( ! H.isLeaf(node) );
									uint64_t const nodeid = node - H.leafs();
									bool const b = E.getBitFromTop(sym,i);

									if ( nodeid == y )
									{
										VRL[nodeid]->putbit(b);
										VNB[nodeid] += 1;
										if ( b )
											VNO[nodeid] += 1;
									}

									if ( b )
										node = H.rightChild(node);
									else
										node = H.leftChild(node);
								}

								assert ( H.isLeaf(node) );
							}
						}

					{
						uint64_t const symfrom = IC.from;
						uint64_t const symto   = IC.from + IC.diameter();
						libmaus2::huffman::RLDecoder dec(std::vector<std::string>(1,infn),symfrom /* offset */,1 /* numthreads */);

						for ( uint64_t z = symfrom; z < symto; ++z )
						{
							int64_t const sym = dec.decode();
							assert ( sym >= 0 );
							unsigned int const codelen = E.getCodeLength(sym);

							uint64_t node = H.root();
							for ( unsigned int i = 0; i < codelen; ++i )
							{
								assert ( ! H.isLeaf(node) );
								uint64_t const nodeid = node - H.leafs();
								bool const b = E.getBitFromTop(sym,i);

								VRL[nodeid]->putbit(b);
								VNB[nodeid] += 1;
								if ( b )
									VNO[nodeid] += 1;

								if ( b )
									node = H.rightChild(node);
								else
									node = H.leftChild(node);
							}

							assert ( H.isLeaf(node) );
						}
					}

					for ( uint64_t y = 0; y < H.inner(); ++y )
						if ( VL[y].first != VL[y].second )
						{
							uint64_t const symfrom = IC.from + IC.diameter();
							uint64_t const symto   = VL[y].second.first;
							libmaus2::huffman::RLDecoder dec(std::vector<std::string>(1,infn),symfrom /* offset */,1 /* numthreads */);

							for ( uint64_t z = symfrom; z < symto; ++z )
							{
								int64_t const sym = dec.decode();
								assert ( sym >= 0 );
								unsigned int const codelen = E.getCodeLength(sym);

								uint64_t node = H.root();
								for ( unsigned int i = 0; i < codelen; ++i )
								{
									assert ( ! H.isLeaf(node) );
									uint64_t const nodeid = node - H.leafs();
									bool const b = E.getBitFromTop(sym,i);

									if ( nodeid == y )
									{
										VRL[nodeid]->putbit(b);
										VNB[nodeid] += 1;
										if ( b )
											VNO[nodeid] += 1;
									}

									if ( b )
										node = H.rightChild(node);
									else
										node = H.leftChild(node);
								}

								assert ( H.isLeaf(node) );
							}
						}

					for ( uint64_t i = 0; i < H.inner(); ++i )
					{
						assert ( VNB[i] == VL[i].second.second - VL[i].first.second );

						uint64_t const symfrom = VL[i].first.first;
						uint64_t const symto = VL[i].second.first;
						std::vector<OneObject> const & LONE = VONE[i];
						std::vector<OneObject>::const_iterator oit =
							std::lower_bound(
								LONE.begin(),
								LONE.end(),
								OneObject(symfrom,symto,0,0,0)
							);
						assert ( oit != LONE.end() && oit->symfrom == static_cast<uint64_t>(symfrom) && oit->symto == static_cast<uint64_t>(symto) );
						assert ( VNO[i] == oit->ocnt );
					}

				}

				for ( uint64_t i = 0; i < H.inner(); ++i )
					if ( VL[i].first != VL[i].second )
					{
						VRL[i]->flushNoHeader(VL[i].second.second == nodebitcnts[i] ? 1 : 0);
						VRL[i].reset();
						VOSI[i]->flush();
						VOSI[i].reset();
					}

				uint64_t const lnodewritecomplete = ++nodewritecomplete;
				libmaus2::aio::StreamLock::lock_type::scope_lock_type slock(libmaus2::aio::StreamLock::cerrlock);
				std::cerr << "[V] " << lnodewritecomplete << "/" << npacks << std::endl;
			}
			catch(std::exception const & ex)
			{
				libmaus2::aio::StreamLock::lock_type::scope_lock_type slock(libmaus2::aio::StreamLock::cerrlock);
				std::cerr << ex.what() << std::endl;
				nodewritefailed = 1;
			}
		}

		if ( nodewritefailed.load() )
		{
			libmaus2::exception::LibMausException lme;
			lme.getStream() << "[E] parallel loop computing node bit packages failed" << std::endl;
			lme.finish();
			throw lme;
		}

		libmaus2::aio::OutputStreamInstance::unique_ptr_type rlhwtCOS(new libmaus2::aio::OutputStreamInstance(rlhwtname));

		uint64_t p = 0;
		p += ::libmaus2::util::NumberSerialisation::serialiseNumber(*rlhwtCOS,n);
		assert ( rlhwtCOS->tellp() == static_cast<int64_t>(p) );
		p += H.serialise(*rlhwtCOS);
		assert ( rlhwtCOS->tellp() == static_cast<int64_t>(p) );
		p += ::libmaus2::util::NumberSerialisation::serialiseNumber(*rlhwtCOS,H.inner());
		assert ( rlhwtCOS->tellp() == static_cast<int64_t>(p) );

		std::vector<uint64_t> nodepos(H.inner());

		#if 0
		for ( uint64_t i = 0; i < H.inner(); ++i )
		{
			nodepos[i] = p;

			uint64_t const fs = libmaus2::util::GetFileSize::getFileSize(rlfilenames[i]);
			libmaus2::aio::InputStreamInstance CIS(rlfilenames[i]);
			libmaus2::util::GetFileSize::copy(CIS,*rlhwtCOS,fs);

			p += fs;
			assert ( rlhwtCOS->tellp() == static_cast<int64_t>(p) );

			libmaus2::aio::FileRemoval::removeFile(rlfilenames[i]);
		}
		#endif


		for ( uint64_t y = 0; y < H.inner(); ++y )
		{
			uint64_t nextbit = 0;
			uint64_t nextsym = 0;
			uint64_t nextid = npacks;

			std::vector<std::string> VTMP;
			std::vector<std::string> VTMPINDEX;

			for ( uint64_t t = 0; t <= npacks; ++t )
			{
				up Vfirst;
				up Vlast ;

				if ( t < npacks )
				{
					uup const & U = VUP[y][t];
					Vfirst = U.first;
					Vlast  = U.second;
				}
				else
				{
					Vfirst = up(n,nodebitcnts[y]);
					Vlast  = up(n+1,nodebitcnts[y]);
				}

				if ( Vfirst != Vlast )
				{
					if ( nextbit < Vfirst.second )
					{
						std::ostringstream tmpfnostr;
						tmpfnostr << tmpfilename << "_" << std::setw(6) << std::setfill('0') << nextid++ << std::setw(0) << "_" << std::setw(6) << y;
						std::string const tmpfn = tmpfnostr.str();
						std::string const tmpfnindex = tmpfn + ".index";
						#if 0
						VTMP[i] = tmpfn;
						VTMPINDEX[i] = tmpfn + ".index";
						#endif

						VTMP.push_back(tmpfn);
						VTMPINDEX.push_back(tmpfnindex);

						libmaus2::aio::OutputStreamInstance::shared_ptr_type sptr(new libmaus2::aio::OutputStreamInstance(tmpfn));
						// libmaus2::aio::InputOutputStreamInstance::shared_ptr_type sinptr(new libmaus2::aio::InputOutputStreamInstance(tmpfnindex,std::ios::in|std::ios::out|std::ios::binary|std::ios::trunc));

						uint64_t const symfrom = nextsym;
						uint64_t const symto   = Vfirst.first;

						std::vector<OneObject> const & LONE = VONE[y];
						std::vector<OneObject>::const_iterator oit =
							std::lower_bound(
								LONE.begin(),
								LONE.end(),
								OneObject(symfrom,symto,0,0,0)
							);

						assert ( oit != LONE.end() && oit->symfrom == static_cast<uint64_t>(symfrom) && oit->symto == static_cast<uint64_t>(symto) );

						libmaus2::rank::RunLengthBitVectorGenerator::shared_ptr_type rptr(
							new libmaus2::rank::RunLengthBitVectorGenerator(
								*sptr,
								tmpfnindex,
								Vfirst.second - nextbit,
								rlblocksize,
								false, /* put header */
								oit->osum
							)
						);

						libmaus2::huffman::RLDecoder dec(std::vector<std::string>(1,infn),symfrom /* offset */,1 /* numthreads */);
						uint64_t nb = 0;
						uint64_t no = 0;

						std::cerr << "Adding missing (" << symfrom << "," << nextbit << ") to (" << symto << "," << Vfirst.second << ")"
							<< " bits " << (Vfirst.second-nextbit)
							<< std::endl;

						for ( uint64_t z = symfrom; z < symto; ++z )
						{
							int64_t const sym = dec.decode();
							assert ( sym >= 0 );
							unsigned int const codelen = E.getCodeLength(sym);

							uint64_t node = H.root();
							for ( unsigned int i = 0; i < codelen; ++i )
							{
								assert ( ! H.isLeaf(node) );
								uint64_t const nodeid = node - H.leafs();
								bool const b = E.getBitFromTop(sym,i);

								if ( nodeid == y )
								{
									rptr->putbit(b);
									nb += 1;
									if ( b )
										no += 1;
								}

								if ( b )
									node = H.rightChild(node);
								else
									node = H.leftChild(node);
							}

							assert ( H.isLeaf(node) );
						}

						assert ( nb == Vfirst.second - nextbit );
						assert ( no == oit->ocnt );

						rptr->flushNoHeader(Vfirst.second == nodebitcnts[y] ? 1 : 0);
						rptr.reset();
						sptr->flush();
						sptr.reset();
					}

					if ( t < npacks )
					{
						if ( Vfirst != Vlast )
						{
							std::cerr << "Should process (" << Vfirst.first << "," << Vfirst.second << ") to (" << Vlast.first << "," << Vlast.second << ")" << std::endl;

							std::string const tmpfn = VVTMP[t][y];
							std::string const indexfn = VVTMPINDEX[t][y];

							VTMP.push_back(tmpfn);
							VTMPINDEX.push_back(indexfn);

							nextsym = Vlast.first;
							nextbit = Vlast.second;
						}

					}
				}
			}

			std::ostringstream fnostr;
			fnostr << tmpfilename << "_" << std::setw(6) << std::setfill('0') << y;
			std::string const fn = fnostr.str();

			libmaus2::aio::OutputStreamInstance::unique_ptr_type pOSI(new libmaus2::aio::OutputStreamInstance(fn));

			uint64_t filepos = 0;

			for ( uint64_t i = 0; i < 3; ++i )
				filepos += libmaus2::util::NumberSerialisation::serialiseNumber(*pOSI,0);
			assert ( pOSI->tellp() == static_cast<int64_t>(filepos) );

			std::vector<uint64_t> Vdatawords;
			uint64_t sumdatawords = 0;

			for ( uint64_t i = 0; i < VTMP.size(); ++i )
			{
				uint64_t const fs = libmaus2::util::GetFileSize::getFileSize(VTMP[i]);
				assert ( fs % sizeof(uint64_t) == 0 );
				uint64_t const words = fs / sizeof(uint64_t);
				Vdatawords.push_back(words);
				sumdatawords += words;
			}

			filepos += libmaus2::autoarray::AutoArray<uint64_t>::putHeader(*pOSI,sumdatawords);
			assert ( pOSI->tellp() == static_cast<int64_t>(filepos) );

			for ( uint64_t i = 0; i < VTMP.size(); ++i )
			{
				libmaus2::aio::InputStreamInstance::unique_ptr_type ISI(new libmaus2::aio::InputStreamInstance(VTMP[i]));
				uint64_t const fs = libmaus2::util::GetFileSize::getFileSize(*ISI);
				ISI->clear();
				ISI->seekg(0);
				libmaus2::util::GetFileSize::copy(*ISI,*pOSI,fs);
				filepos += fs;
				assert ( pOSI->tellp() == static_cast<int64_t>(filepos) );
				ISI.reset();
				libmaus2::aio::FileRemoval::removeFile(VTMP[i]);
			}

			uint64_t const indexpos = filepos;

			std::vector<uint64_t> Vindexwords;
			uint64_t sumindexwords = 0;

			for ( uint64_t i = 0; i < VTMPINDEX.size(); ++i )
			{
				uint64_t const fs = libmaus2::util::GetFileSize::getFileSize(VTMPINDEX[i]);
				assert ( fs % sizeof(uint64_t) == 0 );
				uint64_t const words = fs / sizeof(uint64_t);
				Vindexwords.push_back(words);
				sumindexwords += words;
			}

			filepos += libmaus2::autoarray::AutoArray<uint64_t>::putHeader(*pOSI,sumindexwords);
			assert ( pOSI->tellp() == static_cast<int64_t>(filepos) );

			uint64_t bitoffset = 0;
			for ( uint64_t i = 0; i < VTMPINDEX.size(); ++i )
			{
				uint64_t const fs = libmaus2::util::GetFileSize::getFileSize(VTMPINDEX[i]);
				assert ( fs % sizeof(uint64_t) == 0 );
				uint64_t const words = fs / sizeof(uint64_t);
				libmaus2::aio::InputStreamInstance::unique_ptr_type pISI(new libmaus2::aio::InputStreamInstance(VTMPINDEX[i]));
				for ( uint64_t j = 0; j < words; ++j )
				{
					uint64_t v;
					::libmaus2::serialize::Serialize<uint64_t>::deserializeChecked(*pISI,&v);
					filepos += ::libmaus2::serialize::Serialize<uint64_t>::serializeChecked(*pOSI,v+bitoffset);
				}
				bitoffset += Vdatawords[i] * sizeof(uint64_t) * 8;
				pISI.reset();
				libmaus2::aio::FileRemoval::removeFile(VTMPINDEX[i]);
			}

			pOSI->seekp(0);
			libmaus2::util::NumberSerialisation::serialiseNumber(*pOSI,rlblocksize);
			libmaus2::util::NumberSerialisation::serialiseNumber(*pOSI,nodebitcnts[y]);
			libmaus2::util::NumberSerialisation::serialiseNumber(*pOSI,indexpos);

			pOSI->flush();
			pOSI.reset();

			if ( deepverify )
			{
				libmaus2::rank::RunLengthBitVector::unique_ptr_type prank(
					libmaus2::rank::RunLengthBitVector::load(fn)
				);

				std::vector<uint64_t> VR0(npacks);
				std::vector<uint64_t> VR1(npacks);
				std::vector<uint64_t> VO(npacks);

				#if defined(_OPENMP)
				#pragma omp parallel for num_threads(threads) schedule(dynamic,1)
				#endif
				for ( uint64_t t = 0; t < npacks; ++t )
				{
					uint64_t const low = t*packsize;
					uint64_t const high = std::min(low+packsize,n);

					libmaus2::huffman::RLDecoder dec(std::vector<std::string>(1,infn),low /* offset */,1 /* numthreads */);

					uint64_t r0 = 0;
					uint64_t r1 = 0;
					uint64_t nb = 0;

					for ( uint64_t z = low; z < high; ++z )
					{
						int64_t const sym = dec.decode();
						assert ( sym >= 0 );
						unsigned int const codelen = E.getCodeLength(sym);

						uint64_t node = H.root();
						for ( unsigned int i = 0; i < codelen; ++i )
						{
							assert ( ! H.isLeaf(node) );
							uint64_t const nodeid = node - H.leafs();
							bool const b = E.getBitFromTop(sym,i);

							if ( nodeid == y )
							{
								if ( b )
									r1++;
								else
									r0++;

								nb += 1;
							}

							if ( b )
								node = H.rightChild(node);
							else
								node = H.leftChild(node);
						}

						assert ( H.isLeaf(node) );
					}

					VR0[t] = r0;
					VR1[t] = r1;
					VO[t] = nb;
				}

				libmaus2::util::PrefixSums::prefixSums(VR0.begin(),VR0.end());
				libmaus2::util::PrefixSums::prefixSums(VR1.begin(),VR1.end());
				libmaus2::util::PrefixSums::prefixSums(VO.begin(),VO.end());

				std::atomic<uint64_t> averified(0);

				#if defined(_OPENMP)
				#pragma omp parallel for num_threads(threads) schedule(dynamic,1)
				#endif
				for ( uint64_t t = 0; t < npacks; ++t )
				{
					uint64_t const low = t*packsize;
					uint64_t const high = std::min(low+packsize,n);

					libmaus2::huffman::RLDecoder dec(std::vector<std::string>(1,infn),low /* offset */,1 /* numthreads */);

					uint64_t r0 = VR0[t];
					uint64_t r1 = VR1[t];
					uint64_t off = VO[t];

					for ( uint64_t z = low; z < high; ++z )
					{
						int64_t const sym = dec.decode();
						assert ( sym >= 0 );
						unsigned int const codelen = E.getCodeLength(sym);

						uint64_t node = H.root();
						for ( unsigned int i = 0; i < codelen; ++i )
						{
							assert ( ! H.isLeaf(node) );
							uint64_t const nodeid = node - H.leafs();
							bool const b = E.getBitFromTop(sym,i);

							if ( nodeid == y )
							{
								if ( b )
									r1++;
								else
									r0++;

								assert ( prank->rank1(off) == r1 );
								assert ( prank->rank0(off) == r0 );
								assert ( (*prank)[off++] == b );
								assert ( prank->rankm1(off) == r1 );
								assert ( prank->rankm0(off) == r0 );

								uint64_t const lverified = ++averified;

								if ( lverified % (1024*1024) == 0 )
								{
									libmaus2::aio::StreamLock::lock_type::scope_lock_type slock(libmaus2::aio::StreamLock::cerrlock);
									std::cerr << "[V] " << lverified << "/" << nodebitcnts[y] << std::endl;
								}
							}

							if ( b )
								node = H.rightChild(node);
							else
								node = H.leftChild(node);
						}

						assert ( H.isLeaf(node) );
					}
				}
				std::cerr << "[V] " << averified.load() << "/" << nodebitcnts[y] << std::endl;
			}

			nodepos[y] = p;

			uint64_t const fs = libmaus2::util::GetFileSize::getFileSize(fn);
			libmaus2::aio::InputStreamInstance CIS(fn);
			libmaus2::util::GetFileSize::copy(CIS,*rlhwtCOS,fs);

			p += fs;
			assert ( rlhwtCOS->tellp() == static_cast<int64_t>(p) );

			libmaus2::aio::FileRemoval::removeFile(fn);
		}

		uint64_t const ip = p;
		p += ::libmaus2::util::NumberSerialisation::serialiseNumberVector(*rlhwtCOS,nodepos);
		assert ( rlhwtCOS->tellp() == static_cast<int64_t>(p) );
		p += ::libmaus2::util::NumberSerialisation::serialiseNumber(*rlhwtCOS,ip);
		assert ( rlhwtCOS->tellp() == static_cast<int64_t>(p) );

		rlhwtCOS->flush();
		rlhwtCOS.reset();

		if ( verify )
		{
			libmaus2::aio::InputStreamInstance ISI(rlhwtname);
			ISI.seekg(- static_cast<int64_t>(sizeof(uint64_t)), std::ios::end);
			ISI.clear();
			uint64_t const nodepostablep = libmaus2::util::NumberSerialisation::deserialiseNumber(ISI);
			ISI.clear();
			ISI.seekg(nodepostablep);
			std::vector<uint64_t> Vnodepos = libmaus2::util::NumberSerialisation::deserialiseNumberVector<uint64_t>(ISI);
			assert ( Vnodepos.size() == nodepos.size() );
			assert ( Vnodepos == nodepos );
			assert ( Vnodepos.size() == H.inner() );
			typedef libmaus2::rank::RunLengthBitVector bv_type;
			typedef bv_type::unique_ptr_type bv_ptr;
			libmaus2::autoarray::AutoArray<bv_ptr> ABV(H.inner());

			for ( uint64_t i = 0; i < H.inner(); ++i )
			{
				ISI.clear();
				ISI.seekg(Vnodepos[i],std::ios::beg);
				bv_ptr p(new bv_type(ISI));
				ABV[i] = std::move(p);

				#if 0
				for ( uint64_t j = 0; j < ABV[i]->getNumBlocks(); ++j )
				{
					uint64_t const bbl = ABV[i]->getBlockBitLength(j);
					std::cerr << "dict " << i << " block " << j << "/" << ABV[i]->getNumBlocks() << " bbl " << bbl << std::endl;
				}
				#endif

				double const avg = ABV[i]->getAvgBlockBitLength(threads);
				std::cerr << "[V] node " << i << " avg block bit length " << avg << std::endl;
			}

			ISI.clear();
			ISI.seekg(0);

			uint64_t const nn = libmaus2::util::NumberSerialisation::deserialiseNumber(ISI);
			assert ( nn == n );
			libmaus2::huffman::HuffmanTree RH(ISI);
			assert ( RH == H );
			uint64_t const ndict = libmaus2::util::NumberSerialisation::deserialiseNumber(ISI);
			assert ( ndict == H.inner() );

			for ( uint64_t i = 0; i < H.inner(); ++i )
			{
				assert ( ISI.tellg() == static_cast<int64_t>(Vnodepos[i]) );
				bv_type bv(ISI);
			}

			assert ( ISI.tellg() == static_cast<int64_t>(nodepostablep) );

			std::atomic<uint64_t> afinished(0);

			#if defined(_OPENMP)
			#pragma omp parallel for num_threads(threads) schedule(dynamic,1)
			#endif
			for ( uint64_t t = 0; t < npacks; ++t )
			{
				// std::cerr << "verifying pack " << t << std::endl;
				uint64_t const low = t * packsize;
				uint64_t const high = std::min(low+packsize,n);

				libmaus2::huffman::RLDecoder dec(std::vector<std::string>(1,infn),low /* offset */,1 /* numthreads */);
				libmaus2::huffman::RLDecoder::run_type RL;

				uint64_t pp = low;
				// Vnodecnt[i][t]
				std::vector<uint64_t> VO(H.inner(),0);
				for ( uint64_t i = 0; i < H.inner(); ++i )
					VO[i] = Vnodecnt[i][t];

				while ( dec.decodeRun(RL) && pp < high )
				{
					int64_t const sym = RL.sym;
					uint64_t const cnt = RL.rlen;

					unsigned int const codelen = E.getCodeLength(sym);
					uint64_t node = H.root();

					for ( unsigned int i = 0; i < codelen; ++i )
					{
						assert ( ! H.isLeaf(node) );
						uint64_t const nodeid = node - H.leafs();
						bool const b = E.getBitFromTop(sym,i);

						for ( uint64_t j = 0; j < cnt; ++j )
						{
							uint64_t const lp = VO[nodeid]++;
							assert ( ABV[nodeid]->get(lp) == b );
						}

						if ( b )
							node = H.rightChild(node);
						else
							node = H.leftChild(node);
					}
					assert ( H.isLeaf(node) );

					for ( uint64_t j = 0; j < cnt; ++j )
					{
						uint64_t const lfinished = ++afinished;
						if ( lfinished % (1024*1024) == 0 )
						{
							libmaus2::aio::StreamLock::lock_type::scope_lock_type slock(libmaus2::aio::StreamLock::cerrlock);
							std::cerr << "[V] verified " << lfinished << "/" << n << std::endl;
						}
					}

					pp += cnt;
				}
			}
		}

		std::cerr << "[V] size of rlhwt is " << p << std::endl;
	}


	if ( verify )
	{
		libmaus2::aio::InputStreamInstance rlhwtCIS(rlhwtname);
		libmaus2::wavelet::ImpCompactRLHuffmanWaveletTree ICRLHWT(rlhwtCIS);

		assert ( ICRLHWT.n == n );

		for ( uint64_t i = 0; i < ICRLHWT.dicts.size(); ++i )
		{
			double const avg = ICRLHWT.dicts[i]->getAvgBlockBitLength(threads);
			std::cerr << "node " << i << " avg block bit length "
				<< avg
				<< " (" << avg / 8.0 << " bytes)"
				<< std::endl;

			libmaus2::util::Histogram::unique_ptr_type hist(ICRLHWT.dicts[i]->getRunLengthHistogram(threads));
			hist->printFrac(std::cerr);
		}

		uint64_t const n = ICRLHWT.size();
		uint64_t const packsize = (n + threads - 1)/threads;
		uint64_t const numpacks = (n + packsize-1)/packsize;
		std::atomic<uint64_t> fin(0);
		libmaus2::timing::RealTimeClock rtc;
		rtc.start();

		#if defined(_OPENMP)
		#pragma omp parallel for num_threads(threads)
		#endif
		for ( int64_t t = 0; t < static_cast<int64_t>(numpacks); ++t )
		{
			uint64_t const low = t * packsize;
			uint64_t const high = std::min(low+packsize,n);

			libmaus2::huffman::RLDecoder debdec(std::vector<std::string>(1,infn),low,1 /* numthreads */);

			for ( uint64_t i = low; i < high; ++i )
			{
				int64_t const sym = ICRLHWT[i];
				int64_t const debsym = debdec.decode();
				assert ( sym == debsym );

				uint64_t lfin = ++fin;
				if ( lfin % (1024*1024) == 0 )
				{
					std::cerr
						<< "(" << static_cast<double>(lfin)/n << ")"
						<< " " << (lfin/rtc.getElapsedSeconds())/1.0e6
						<< std::endl;
				}
			}
		}

		std::cerr
			<< "(" << static_cast<double>(fin.load())/n << ")"
			<< " " << (fin.load()/rtc.getElapsedSeconds())/1.0e6
			<< std::endl;

		std::cerr << "[V] verification complete" << std::endl;
	}
}

int main(int argc, char * argv[])
{
	try
	{
		::libmaus2::util::ArgInfo const arginfo(argc,argv);

		if ( arginfo.helpRequested() || arginfo.restargs.size() < 1 )
		{
			::libmaus2::exception::LibMausException se;
			std::ostream & str = se.getStream();
			str << "usage: " << argv[0] << " <in.bwt>" << std::endl;
			str << std::endl;

			se.finish();
			throw se;
		}

		bwtToRlHwtPar(arginfo);

		return EXIT_SUCCESS;
	}
	catch(std::exception const & ex)
	{
		std::cerr << ex.what() << std::endl;
		return EXIT_FAILURE;
	}
}
