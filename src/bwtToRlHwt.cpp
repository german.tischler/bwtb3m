/**
    bwtb3m
    Copyright (C) 2009-2014 German Tischler
    Copyright (C) 2011-2014 Genome Research Limited

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
#include <libmaus2/huffman/RLDecoder.hpp>
#include <libmaus2/util/ArgInfo.hpp>
#include <libmaus2/util/utf8.hpp>
#include <libmaus2/util/NumberMapSerialisation.hpp>
#include <libmaus2/util/OutputFileNameTools.hpp>
#include <libmaus2/util/TempFileRemovalContainer.hpp>
#include <libmaus2/huffman/HuffmanTree.hpp>
#include <libmaus2/rank/RunLengthBitVectorGenerator.hpp>
#include <libmaus2/rank/RunLengthBitVector.hpp>
#include <libmaus2/wavelet/ImpCompactHuffmanWaveletTree.hpp>
#include <libmaus2/timing/RealTimeClock.hpp>
#include <libmaus2/aio/InputOutputStreamInstance.hpp>
#include <iostream>

void hwtToRlHwt(::libmaus2::util::ArgInfo const & arginfo)
{
	uint64_t const rlblocksize = arginfo.getValueUnsignedNumeric<uint64_t>("blocksize",64*1024);
	bool const recreate = arginfo.getValue<unsigned int>("recreate",true);

	std::string const infn = arginfo.getRestArg<std::string>(0);
        std::string const rlhwtname = libmaus2::util::OutputFileNameTools::clipOff(infn,".bwt") + ".rlhwt";
	bool const verify = arginfo.getValue<unsigned int>("verify",false);
	uint64_t const threads = arginfo.getValueUnsignedNumeric<uint64_t>("threads",1);

	std::string const histfn = libmaus2::util::OutputFileNameTools::clipOff(infn,".bwt") + ".hist";

	libmaus2::aio::InputStreamInstance histstr(histfn);
	std::map<int64_t,uint64_t> const hist = ::libmaus2::util::NumberMapSerialisation::deserialiseMap<std::istream,int64_t,uint64_t>(histstr);
	std::vector<uint64_t> syms;
	std::vector<std::pair<int64_t,uint64_t> > symfreq;
	uint64_t n = 0;

	for ( std::map<int64_t,uint64_t>::const_iterator ita = hist.begin();
		ita != hist.end(); ++ita )
	{
		symfreq.push_back(std::make_pair(ita->first,ita->second));
		std::cerr << "[V] H[" << ita->first << "]=" << ita->second << std::endl;
		syms.push_back(ita->first);
		n += ita->second;
	}

	libmaus2::huffman::HuffmanTree H(symfreq.begin(),symfreq.size(),false,true);

	std::cerr << H.toString();

	libmaus2::huffman::HuffmanTree::EncodeTable E(H);

	for ( uint64_t i = 0; i < symfreq.size(); ++i )
	{
		int64_t const sym = symfreq[i].first;
		unsigned int const codelen = E.getCodeLength(sym);
		std::cerr << "[V] C[" << sym << "]="; // << E.printCode(sym) << std::endl;

		for ( unsigned int j = 0; j < codelen; ++j )
		{
			bool const b = E.getBitFromTop(sym,j);
			std::cerr << b;
		}
		std::cerr << std::endl;
	}

	std::cerr << "Number of inner nodes " << H.inner() << " root " << H.root() << std::endl;

	std::vector<uint64_t> nodebitcnts(H.inner());

	for ( uint64_t i = 0; i < syms.size(); ++i )
	{
		uint64_t const s = syms[i];
		assert ( hist.find(s) != hist.end() );
		uint64_t const f = hist.find(s)->second;
		unsigned int const codelen = E.getCodeLength(s);
		uint64_t node = H.root();

		for ( unsigned int j = 0; j < codelen; ++j )
		{
			uint64_t const nodeid = node - H.leafs();
			nodebitcnts.at(nodeid) += f;

			bool const b = E.getBitFromTop(s,j);

			assert ( !H.isLeaf(node) );

			if ( b )
				node = H.rightChild(node);
			else
				node = H.leftChild(node);
		}

		assert ( H.isLeaf(node) );
	}

	for ( uint64_t i = 0; i < nodebitcnts.size(); ++i )
		std::cerr << "[V] nodebitcnts[" << i << "]=" << nodebitcnts[i] << std::endl;



        if ( (! libmaus2::util::GetFileSize::fileExists(rlhwtname)) || recreate )
        {
		std::string const tmpfileprefix = arginfo.getDefaultTmpFileName();
		std::string const rlfnprefix = tmpfileprefix + "_rl";
		libmaus2::util::TempFileRemovalContainer::setup();
		std::vector<std::string> rlfilenames(H.inner());
		std::vector<std::string> rlidxfilenames(H.inner());
		libmaus2::autoarray::AutoArray<libmaus2::aio::OutputStreamInstance::unique_ptr_type> rloutfiles(H.inner());
		// libmaus2::autoarray::AutoArray<libmaus2::aio::InputOutputStreamInstance::unique_ptr_type> rlidxoutfiles(H.inner());
		libmaus2::autoarray::AutoArray<libmaus2::rank::RunLengthBitVectorGenerator::unique_ptr_type> rlgens(H.inner());
		for ( uint64_t i = 0; i < H.inner(); ++i )
		{
			std::ostringstream fnostr;
			fnostr << rlfnprefix << "_" << std::setw(6) << std::setfill('0') << i << std::setw(0);
			rlfilenames[i] = fnostr.str() + ".rl";
			rlidxfilenames[i] = fnostr.str() + ".rlidx";

			libmaus2::util::TempFileRemovalContainer::addTempFile(rlfilenames[i]);
			libmaus2::util::TempFileRemovalContainer::addTempFile(rlidxfilenames[i]);

			libmaus2::aio::OutputStreamInstance::unique_ptr_type trl(
				new libmaus2::aio::OutputStreamInstance(rlfilenames[i])
			);
			rloutfiles[i] = std::move(trl);

			#if 0
			libmaus2::aio::InputOutputStreamInstance::unique_ptr_type trlidx(
				new libmaus2::aio::InputOutputStreamInstance(rlidxfilenames[i],std::ios::in|std::ios::out|std::ios::trunc|std::ios::binary)
			);
			rlidxoutfiles[i] = std::move(trlidx);
			#endif

			libmaus2::rank::RunLengthBitVectorGenerator::unique_ptr_type tgen(
				new libmaus2::rank::RunLengthBitVectorGenerator(
					*rloutfiles[i],
					rlidxfilenames[i],
					nodebitcnts[i],
					rlblocksize
				)
			);

			rlgens[i] = std::move(tgen);
		}

		uint64_t const tpacks = 256 * threads;
		uint64_t const packsize = tpacks ? ((n + tpacks - 1) / tpacks) : 0;
		uint64_t const npacks = packsize ? ((n + packsize - 1)) / packsize : 0;

		libmaus2::huffman::RLDecoder dec(std::vector<std::string>(1,infn),0 /* offset */,1 /* numthreads */);
		libmaus2::huffman::RLDecoder::run_type RL;
		std::vector<uint64_t> VO(H.inner());
		std::vector<std::vector<uint64_t> > VVO;
		uint64_t fin = 0;
		while ( dec.decodeRun(RL) )
		{
			int64_t const sym = RL.sym;
			uint64_t const cnt = RL.rlen;
			unsigned int const codelen = E.getCodeLength(sym);

			for ( uint64_t j = 0; j < cnt; ++j )
			{
				if ( fin % packsize == 0 )
				{
					VVO.push_back(VO);
				}

				uint64_t node = H.root();
				for ( unsigned int i = 0; i < codelen; ++i )
				{
					assert ( ! H.isLeaf(node) );
					uint64_t const nodeid = node - H.leafs();
					bool const b = E.getBitFromTop(sym,i);

					rlgens[nodeid]->putbit(b);
					VO[nodeid]++;

					if ( b )
						node = H.rightChild(node);
					else
						node = H.leftChild(node);
				}

				assert ( H.isLeaf(node) );

				if ( ++fin % (128*1024*1024) == 0 )
				{
					if ( isatty(STDERR_FILENO) )
						std::cerr << '\r' << std::string(60,' ') << '\r' <<
							static_cast<double>(fin)/n << "\t" << fin << "/" << n << std::flush;
					else
						std::cerr << static_cast<double>(fin)/n << "\t" << fin << "/" << n << '\n';
				}
			}
		}

		if ( isatty(STDERR_FILENO) )
			std::cerr << '\r' << std::string(60,' ') << '\r' <<
				static_cast<double>(fin)/n << "\t" << fin << "/" << n << std::endl;
		else
			std::cerr << static_cast<double>(fin)/n << "\t" << fin << "/" << n << std::endl;


		for ( uint64_t i = 0; i < H.inner(); ++i )
		{
			rlgens[i]->flush();
			rlgens[i].reset();
			#if 0
			rlidxoutfiles[i]->flush();
			rlidxoutfiles[i].reset();
			#endif
			rloutfiles[i]->flush();
			rloutfiles[i].reset();
			libmaus2::aio::FileRemoval::removeFile(rlidxfilenames[i]);
		}

		libmaus2::aio::OutputStreamInstance::unique_ptr_type rlhwtCOS(new libmaus2::aio::OutputStreamInstance(rlhwtname));

		uint64_t p = 0;
		p += ::libmaus2::util::NumberSerialisation::serialiseNumber(*rlhwtCOS,n);
		assert ( rlhwtCOS->tellp() == static_cast<int64_t>(p) );
		p += H.serialise(*rlhwtCOS);
		assert ( rlhwtCOS->tellp() == static_cast<int64_t>(p) );
		p += ::libmaus2::util::NumberSerialisation::serialiseNumber(*rlhwtCOS,H.inner());
		assert ( rlhwtCOS->tellp() == static_cast<int64_t>(p) );

		std::vector<uint64_t> nodepos(H.inner());
		for ( uint64_t i = 0; i < H.inner(); ++i )
		{
			nodepos[i] = p;

			uint64_t const fs = libmaus2::util::GetFileSize::getFileSize(rlfilenames[i]);
			libmaus2::aio::InputStreamInstance CIS(rlfilenames[i]);
			libmaus2::util::GetFileSize::copy(CIS,*rlhwtCOS,fs);

			p += fs;
			assert ( rlhwtCOS->tellp() == static_cast<int64_t>(p) );

			libmaus2::aio::FileRemoval::removeFile(rlfilenames[i]);
		}

		uint64_t const ip = p;
		p += ::libmaus2::util::NumberSerialisation::serialiseNumberVector(*rlhwtCOS,nodepos);
		assert ( rlhwtCOS->tellp() == static_cast<int64_t>(p) );
		p += ::libmaus2::util::NumberSerialisation::serialiseNumber(*rlhwtCOS,ip);
		assert ( rlhwtCOS->tellp() == static_cast<int64_t>(p) );

		rlhwtCOS->flush();
		rlhwtCOS.reset();

		if ( verify )
		{
			libmaus2::aio::InputStreamInstance ISI(rlhwtname);
			ISI.seekg(- static_cast<int64_t>(sizeof(uint64_t)), std::ios::end);
			ISI.clear();
			uint64_t const nodepostablep = libmaus2::util::NumberSerialisation::deserialiseNumber(ISI);
			ISI.clear();
			ISI.seekg(nodepostablep);
			std::vector<uint64_t> Vnodepos = libmaus2::util::NumberSerialisation::deserialiseNumberVector<uint64_t>(ISI);
			assert ( Vnodepos.size() == nodepos.size() );
			assert ( Vnodepos == nodepos );
			assert ( Vnodepos.size() == H.inner() );
			typedef libmaus2::rank::RunLengthBitVector bv_type;
			typedef bv_type::unique_ptr_type bv_ptr;
			libmaus2::autoarray::AutoArray<bv_ptr> ABV(H.inner());

			for ( uint64_t i = 0; i < H.inner(); ++i )
			{
				ISI.clear();
				ISI.seekg(Vnodepos[i],std::ios::beg);
				bv_ptr p(new bv_type(ISI));
				ABV[i] = std::move(p);

				#if 0
				for ( uint64_t j = 0; j < ABV[i]->getNumBlocks(); ++j )
				{
					uint64_t const bbl = ABV[i]->getBlockBitLength(j);
					std::cerr << "dict " << i << " block " << j << "/" << ABV[i]->getNumBlocks() << " bbl " << bbl << std::endl;
				}
				#endif

				double const avg = ABV[i]->getAvgBlockBitLength(threads);
				std::cerr << "[V] node " << i << " avg block bit length " << avg << std::endl;
			}

			ISI.clear();
			ISI.seekg(0);

			uint64_t const nn = libmaus2::util::NumberSerialisation::deserialiseNumber(ISI);
			assert ( nn == n );
			libmaus2::huffman::HuffmanTree RH(ISI);
			assert ( RH == H );
			uint64_t const ndict = libmaus2::util::NumberSerialisation::deserialiseNumber(ISI);
			assert ( ndict == H.inner() );

			for ( uint64_t i = 0; i < H.inner(); ++i )
			{
				assert ( ISI.tellg() == static_cast<int64_t>(Vnodepos[i]) );
				bv_type bv(ISI);
			}

			assert ( ISI.tellg() == static_cast<int64_t>(nodepostablep) );

			std::atomic<uint64_t> afinished(0);

			#if defined(_OPENMP)
			#pragma omp parallel for num_threads(threads) schedule(dynamic,1)
			#endif
			for ( uint64_t t = 0; t < npacks; ++t )
			{
				// std::cerr << "verifying pack " << t << std::endl;
				uint64_t const low = t * packsize;
				uint64_t const high = std::min(low+packsize,n);

				libmaus2::huffman::RLDecoder dec(std::vector<std::string>(1,infn),low /* offset */,1 /* numthreads */);
				libmaus2::huffman::RLDecoder::run_type RL;

				uint64_t pp = low;
				std::vector<uint64_t> VO = VVO[t];
				while ( dec.decodeRun(RL) && pp < high )
				{
					int64_t const sym = RL.sym;
					uint64_t const cnt = RL.rlen;

					unsigned int const codelen = E.getCodeLength(sym);
					uint64_t node = H.root();

					for ( unsigned int i = 0; i < codelen; ++i )
					{
						assert ( ! H.isLeaf(node) );
						uint64_t const nodeid = node - H.leafs();
						bool const b = E.getBitFromTop(sym,i);

						for ( uint64_t j = 0; j < cnt; ++j )
						{
							uint64_t const lp = VO[nodeid]++;
							assert ( ABV[nodeid]->get(lp) == b );
						}

						if ( b )
							node = H.rightChild(node);
						else
							node = H.leftChild(node);
					}
					assert ( H.isLeaf(node) );

					for ( uint64_t j = 0; j < cnt; ++j )
					{
						uint64_t const lfinished = ++afinished;
						if ( lfinished % (1024*1024) == 0 )
						{
							libmaus2::aio::StreamLock::lock_type::scope_lock_type slock(libmaus2::aio::StreamLock::cerrlock);
							std::cerr << "[V] verified " << lfinished << "/" << n << std::endl;
						}
					}

					pp += cnt;
				}
			}
		}

		std::cerr << "[V] size of rlhwt is " << p << std::endl;
	}


	if ( verify )
	{
		libmaus2::aio::InputStreamInstance rlhwtCIS(rlhwtname);
		libmaus2::wavelet::ImpCompactRLHuffmanWaveletTree ICRLHWT(rlhwtCIS);

		assert ( ICRLHWT.n == n );

		for ( uint64_t i = 0; i < ICRLHWT.dicts.size(); ++i )
		{
			double const avg = ICRLHWT.dicts[i]->getAvgBlockBitLength(threads);
			std::cerr << "node " << i << " avg block bit length "
				<< avg
				<< " (" << avg / 8.0 << " bytes)"
				<< std::endl;

			libmaus2::util::Histogram::unique_ptr_type hist(ICRLHWT.dicts[i]->getRunLengthHistogram(threads));
			hist->printFrac(std::cerr);
		}

		uint64_t const n = ICRLHWT.size();
		uint64_t const packsize = (n + threads - 1)/threads;
		uint64_t const numpacks = (n + packsize-1)/packsize;
		libmaus2::parallel::SynchronousCounter<uint64_t> fin(0);
		libmaus2::timing::RealTimeClock rtc;
		rtc.start();

		#if defined(_OPENMP)
		#pragma omp parallel for num_threads(threads)
		#endif
		for ( int64_t t = 0; t < static_cast<int64_t>(numpacks); ++t )
		{
			uint64_t const low = t * packsize;
			uint64_t const high = std::min(low+packsize,n);

			libmaus2::huffman::RLDecoder debdec(std::vector<std::string>(1,infn),low,1 /* numthreads */);

			for ( uint64_t i = low; i < high; ++i )
			{
				int64_t const sym = ICRLHWT[i];
				int64_t const debsym = debdec.decode();
				assert ( sym == debsym );

				uint64_t lfin;
				if ( (lfin=++fin) % (1024*1024) == 0 )
				{
					std::cerr
						<< '\r' << std::string(80,' ') << '\r' << lfin/(1024*1024)
						<< " (" << static_cast<double>(lfin)/n << ")"
						<< " " << (lfin/rtc.getElapsedSeconds())/1.0e6
						<< std::flush;
				}
			}
		}

		std::cerr
			<< '\r' << std::string(80,' ') << '\r' << fin.get()
			<< " (" << static_cast<double>(fin.get())/n << ")"
			<< " " << (fin.get()/rtc.getElapsedSeconds())/1.0e6
			<< std::endl;
	}
}

int main(int argc, char * argv[])
{
	try
	{
		::libmaus2::util::ArgInfo const arginfo(argc,argv);

		if ( arginfo.helpRequested() || arginfo.restargs.size() < 1 )
		{
			::libmaus2::exception::LibMausException se;
			std::ostream & str = se.getStream();
			str << "usage: " << argv[0] << " <in.bwt>" << std::endl;
			str << std::endl;

			se.finish();
			throw se;
		}

		hwtToRlHwt(arginfo);

		return EXIT_SUCCESS;
	}
	catch(std::exception const & ex)
	{
		std::cerr << ex.what() << std::endl;
		return EXIT_FAILURE;
	}
}
