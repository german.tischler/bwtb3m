/**
    bwtb3m
    Copyright (C) 2018 German Tischler-Höhle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
#include <libmaus2/bitio/CompactArrayWriterFile.hpp>
#include <libmaus2/fastx/StreamFastAReader.hpp>
#include <libmaus2/aio/InputStreamFactoryContainer.hpp>
#include <libmaus2/util/OutputFileNameTools.hpp>
#include <libmaus2/util/ArgParser.hpp>
#include <libmaus2/sorting/SortingBufferedOutputFile.hpp>
#include <libmaus2/bitio/CompactDecoderBuffer.hpp>

struct Entry
{
	uint64_t uid;
	std::string id;
	std::string data;

	Entry()
	{

	}

	Entry(uint64_t const ruid, std::string const & rid, std::string const & rdata)
	: uid(ruid), id(rid), data(rdata) {}

	std::ostream & serialise(std::ostream & out) const
	{
		libmaus2::util::NumberSerialisation::serialiseNumber(out,uid);
		libmaus2::util::StringSerialisation::serialiseString(out,id);
		libmaus2::util::StringSerialisation::serialiseString(out,data);
		return out;
	}

	std::istream & deserialise(std::istream & in)
	{
		uid = libmaus2::util::NumberSerialisation::deserialiseNumber(in);
		id = libmaus2::util::StringSerialisation::deserialiseString(in);
		data = libmaus2::util::StringSerialisation::deserialiseString(in);
		return in;
	}

	bool operator<(Entry const & E) const
	{
		return uid < E.uid;
	}
};

int fagzToCompact4BiDir(libmaus2::util::ArgParser const & arg)
{
	std::string const output = arg[0];

	std::vector<std::string> inputfilenames;
	for ( uint64_t i = 1; i < arg.size(); ++i )
		inputfilenames.push_back(arg[i]);

	std::string const ftmp = output + "_f";
	std::string const rtmp = output + "_r";

	libmaus2::util::TempFileRemovalContainer::addTempFile(ftmp);
	libmaus2::util::TempFileRemovalContainer::addTempFile(rtmp);

	libmaus2::aio::OutputStreamInstance::unique_ptr_type fOSI(new libmaus2::aio::OutputStreamInstance(ftmp));
	libmaus2::aio::OutputStreamInstance::unique_ptr_type rOSI(new libmaus2::aio::OutputStreamInstance(rtmp));

	std::string const x(1,5);
	std::string const y(1,6);

	libmaus2::autoarray::AutoArray<uint8_t> C(7);
	C[0] = 3;
	C[1] = 2;
	C[2] = 1;
	C[3] = 0;
	C[4] = 4;
	C[5] = 6;
	C[6] = 5;

	uint64_t iid = 0;
	uint64_t l = 0;
	for ( uint64_t i = 0; i < inputfilenames.size(); ++i )
	{
		libmaus2::aio::InputStreamInstance ISI(inputfilenames[i]);
		libmaus2::fastx::StreamFastAReaderWrapper SF(ISI);
		libmaus2::fastx::Pattern pat;

		while ( SF.getNextPatternUnlocked(pat) )
		{
			uint64_t const rid = std::numeric_limits<uint64_t>::max() - iid;

			std::string mapped = x + libmaus2::fastx::mapString(pat.spattern) + y;

			Entry(iid,pat.sid,mapped).serialise(*fOSI);

			std::reverse(mapped.begin(),mapped.end());
			for ( uint64_t j = 0; j < mapped.size(); ++j )
				mapped[j] = C[mapped[j]];

			Entry(rid,pat.sid,mapped).serialise(*rOSI);

			iid += 1;

			l += 2*mapped.size();

			if ( iid % (1024*1024) == 0 )
				std::cerr << "[V]\t" << iid << "\t" << l << std::endl;
		}
	}
	std::cerr << "[V]\t" << iid << "\t" << l << std::endl;

	fOSI->flush();
	fOSI.reset();
	rOSI->flush();
	rOSI.reset();

	libmaus2::sorting::SerialisingSortingBufferedOutputFile<Entry>::sort(rtmp);

	libmaus2::bitio::CompactArrayWriterFile::unique_ptr_type compactout(new libmaus2::bitio::CompactArrayWriterFile(output,3 /* bits per symbol */));

	std::vector<std::string> Vtmp;
	Vtmp.push_back(ftmp);
	Vtmp.push_back(rtmp);

	for ( uint64_t i = 0; i < Vtmp.size(); ++i )
	{
		libmaus2::aio::InputStreamInstance ISI(Vtmp[i]);
		Entry E;

		while ( ISI.peek() != std::istream::traits_type::eof() )
		{
			E.deserialise(ISI);
			std::string const & s = E.data;
			compactout->write(s.c_str(),s.size());
		}
	}

	compactout->flush();
	compactout.reset();

	#if 0
	libmaus2::bitio::CompactDecoderWrapper in(output);
	while ( in.peek() != std::istream::traits_type::eof() )
	{
		int const c = in.get();
		std::cerr << c;
	}

	std::cerr << std::endl;
	#endif

	return EXIT_SUCCESS;
}

int main(int argc, char * argv[])
{
	try
	{
		libmaus2::util::ArgParser const arg(argc,argv);
		return fagzToCompact4BiDir(arg);
	}
	catch(std::exception const & ex)
	{
		std::cerr << ex.what() << std::endl;
		return EXIT_FAILURE;
	}
}
