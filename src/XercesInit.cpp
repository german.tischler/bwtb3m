/**
    bwtb3m
    Copyright (C) 2020 German Tischler-Höhle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
#include <XercesInit.hpp>

XercesInit::XercesInit()
{
	xercesc::XMLPlatformUtils::Initialize();
}

XercesInit::~XercesInit()
{
	xercesc::XMLPlatformUtils::Terminate();
}

xercesc::DOMImplementation * XercesInit::getImplementation()
{
	xercesc::DOMImplementation * domimpl =
		xercesc::DOMImplementationRegistry::getDOMImplementation(nullptr);

	return domimpl;
}

xercesc::XMLTransService * XercesInit::getTransService()
{
	return xercesc::XMLPlatformUtils::fgTransService;
}

std::unique_ptr<XMLTranscoder> XercesInit::getTranscoder()
{
	xercesc::XMLTransService::Codes resValue;
	uint64_t const bs = 1024; /* block size */
	xercesc::XMLTranscoder * trans = getTransService()->makeNewTranscoderFor("utf-8",resValue,bs);
	if ( resValue != xercesc::XMLTransService::Ok )
	{
		libmaus2::exception::LibMausException lme;
		lme.getStream() << "[E] failed to get transcoder for utf-8" << std::endl;
		lme.finish();
		throw lme;
	}

	std::unique_ptr<XMLTranscoder> tptr(
		new XMLTranscoder(trans)
	);

	return tptr;
}

std::unique_ptr<DOMDocument> XercesInit::getDocument(std::string const & rootname, xercesc::DOMImplementation * impl,  XMLTranscoder & trans)
{
	libmaus2::autoarray::AutoArray< ::XMLCh > x(trans.transcode(rootname));
	uint64_t const xl = x.size();
	x.resize(xl+1);
	x[xl] = 0;

	std::unique_ptr<DOMDocument> tptr(
		new DOMDocument(
			impl->createDocument(nullptr,x.begin(),nullptr)
		)
	);

	return tptr;
}

std::unique_ptr<DOMLSSerialiser> XercesInit::getSerialiser(xercesc::DOMImplementation * impl)
{
	std::unique_ptr<DOMLSSerialiser> tptr(new DOMLSSerialiser(impl->createLSSerializer()));

	return tptr;
}

std::unique_ptr<DOMLSParser> XercesInit::getParser(xercesc::DOMImplementation * impl)
{
	xercesc::DOMLSParser * parser = impl->createLSParser(xercesc::DOMImplementationLS::MODE_SYNCHRONOUS,nullptr);
	std::unique_ptr<DOMLSParser> tptr(new DOMLSParser(parser));
	return tptr;
}

std::unique_ptr<DOMDocument> XercesInit::parseDocument(std::string const & data, DOMLSParser & parser)
{
	libmaus2::autoarray::AutoArray< ::XMLByte > A(data.size());
	std::copy(data.begin(),data.end(),A.begin());
	xercesc::MemBufInputSource memIn(A.begin(),A.size(),static_cast<char const *>(nullptr));
	xercesc::Wrapper4InputSource W4IS(&memIn,false /* adopt */);
	std::unique_ptr<DOMDocument> udoc(
		new DOMDocument(
			parser.parser->parse(&W4IS),
			false /* delete on destruct */
		)
	);
	return udoc;
}

std::unique_ptr<DOMDocument> XercesInit::parseDocument(xercesc::InputSource & IS, DOMLSParser & parser)
{
	xercesc::Wrapper4InputSource wrapper(&IS,false /* adopt */);
	std::unique_ptr<DOMDocument> udoc(
		new DOMDocument(parser.parser->parse(&wrapper),false /* delete on destruct */)
	);
	return udoc;
}
