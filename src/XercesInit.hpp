/**
    bwtb3m
    Copyright (C) 2020 German Tischler-Höhle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
#if ! defined(XERCESINIT_HPP)
#define XERCESINIT_HPP

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/dom/DOMImplementationRegistry.hpp>
#include <xercesc/dom/DOMImplementation.hpp>
#include <xercesc/util/TransService.hpp>
#include <xercesc/dom/DOMDocument.hpp>
#include <memory>
#include <XMLTranscoder.hpp>
#include <DOMDocument.hpp>
#include <DOMLSSerialiser.hpp>
#include <DOMLSParser.hpp>
#include <xercesc/framework/MemBufInputSource.hpp>
#include <xercesc/framework/Wrapper4InputSource.hpp>

struct XercesInit
{
	XercesInit();
	~XercesInit();

	static xercesc::DOMImplementation * getImplementation();
	static xercesc::XMLTransService * getTransService();
	static std::unique_ptr<XMLTranscoder> getTranscoder();
	static std::unique_ptr<DOMDocument> getDocument(std::string const & rootname, xercesc::DOMImplementation * impl,  XMLTranscoder & trans);
	static std::unique_ptr<DOMLSSerialiser> getSerialiser(xercesc::DOMImplementation * impl);
	static std::unique_ptr<DOMLSParser> getParser(xercesc::DOMImplementation * impl);
	static std::unique_ptr<DOMDocument> parseDocument(std::string const & data, DOMLSParser & parser);
	static std::unique_ptr<DOMDocument> parseDocument(xercesc::InputSource & IS, DOMLSParser & parser);
};
#endif
