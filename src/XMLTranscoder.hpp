/**
    bwtb3m
    Copyright (C) 2020 German Tischler-Höhle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
#if ! defined(XMLTRANSCODER_HPP)
#define XMLTRANSCODER_HPP

#include <xercesc/util/TransService.hpp>
#include <libmaus2/autoarray/AutoArray.hpp>

struct XMLTranscoder
{
	xercesc::XMLTranscoder * trans;
	libmaus2::autoarray::AutoArray< ::XMLByte > TA;

	XMLTranscoder(xercesc::XMLTranscoder * rtrans);
	~XMLTranscoder();

	libmaus2::autoarray::AutoArray< ::XMLCh > nullTerminate(libmaus2::autoarray::AutoArray< ::XMLCh > A);
	libmaus2::autoarray::AutoArray< ::XMLCh > transcode(std::string const & s);
	libmaus2::autoarray::AutoArray< ::XMLCh > transcodeNullTerminate(std::string const & s);
	std::string transcode(::XMLCh const * x, uint64_t const s);
	std::string transcode(::XMLCh const * x);
};
#endif
