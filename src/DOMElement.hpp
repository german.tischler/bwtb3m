/**
    bwtb3m
    Copyright (C) 2020 German Tischler-Höhle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
#if ! defined(DOMELEMENT_HPP)
#define DOMELEMENT_HPP

#include <XMLTranscoder.hpp>
#include <xercesc/dom/DOMDocument.hpp>
#include <xercesc/dom/DOMElement.hpp>
#include <xercesc/dom/DOMText.hpp>
#include <xercesc/dom/DOMNamedNodeMap.hpp>
#include <xercesc/dom/DOMAttr.hpp>

struct DOMElement
{
	xercesc::DOMElement * el;

	DOMElement(xercesc::DOMElement * rel);
	~DOMElement();

	void append(xercesc::DOMElement * parent);
	template<typename type>
	void setAttributeTemplate(std::string const & key, type const & value, XMLTranscoder & trans);
	void setAttribute(std::string const & key, std::string const & value, XMLTranscoder & trans);
	static void setAttribute(xercesc::DOMElement * el, std::string const & key, std::string const & value, XMLTranscoder & trans);

	static std::string getTagName(xercesc::DOMElement const * el, XMLTranscoder & trans);
	std::string getTagName(XMLTranscoder & trans) const;
	static std::vector<xercesc::DOMElement *> getElementsByTagName(xercesc::DOMElement * el, std::string const & tagname, XMLTranscoder & trans);
	static std::vector<xercesc::DOMElement *> getChildElements(xercesc::DOMElement * el);
	static std::string getAttribute(xercesc::DOMElement * el, std::string const & attrname, XMLTranscoder & trans);
	static bool hasAttribute(xercesc::DOMElement * el, std::string const & attrname, XMLTranscoder & trans);
	static std::string getText(xercesc::DOMElement * el, XMLTranscoder & trans);
	static std::string getTextRec(xercesc::DOMElement * el, XMLTranscoder & trans);
	static std::vector<xercesc::DOMElement *> getElementsByTagName(xercesc::DOMElement * el, std::string const & tagname, std::string const & attrname, std::string const & attrvalue, XMLTranscoder & trans);
	static std::vector<xercesc::DOMElement *> getElementsByTagNameCascade(xercesc::DOMElement * el, std::vector<std::string> const & Vtagname, XMLTranscoder & trans);

	template<typename type>
	static type getTextForChildParsed(xercesc::DOMElement * el, std::string const & tagname, XMLTranscoder & trans)
	{
		std::istringstream istr(getTextForChild(el,tagname,trans));
		type t;
		istr >> t;
		return t;
	}

	static std::string getTextForChild(xercesc::DOMElement * el, std::string const & tagname, XMLTranscoder & trans)
	{
		std::vector<xercesc::DOMElement *> VD = getElementsByTagName(el,tagname,trans);
		std::ostringstream ostr;
		for ( uint64_t i = 0; i < VD.size(); ++i )
			ostr << getText(VD[i],trans);
		return ostr.str();
	}

	template<typename type>
	static type getAttributeParsed(xercesc::DOMElement * el, std::string const & attrname, XMLTranscoder & trans)
	{
		std::string const arg = getAttribute(el,attrname,trans);
		std::istringstream istr(arg);
		type t;
		istr >> t;

		if ( istr && istr.peek() == std::istream::traits_type::eof() )
			return t;
		else
		{
			libmaus2::exception::LibMausException lme;
			lme.getStream() << "[E] getAttribute(" << getTagName(el,trans) << "," << attrname << "): cannot parse " << arg << std::endl;
			lme.finish();
			throw lme;
		}
	}

	static xercesc::DOMElement * getElementOrCreate(
		xercesc::DOMDocument * doc,
		xercesc::DOMElement * parent,
		std::string const & tag,
		XMLTranscoder & trans
	)
	{
		std::vector<xercesc::DOMElement *> V = DOMElement::getElementsByTagName(parent,tag,trans);

		if ( !V.size() )
		{
			libmaus2::autoarray::AutoArray< ::XMLCh > const A = trans.transcodeNullTerminate(tag.c_str());
			xercesc::DOMElement * xnewel = doc->createElement(A.begin());
			DOMElement newel(xnewel);
			newel.append(parent);
			V = DOMElement::getElementsByTagName(parent,tag,trans);
		}

		assert ( V.size() );

		return V[0];
	}

	static xercesc::DOMElement * getElementOrCreate(
		xercesc::DOMDocument * doc,
		xercesc::DOMElement * parent,
		std::string const & tag,
		std::string const & attrkey,
		std::string const & attrvalue,
		XMLTranscoder & trans
	)
	{
		std::vector<xercesc::DOMElement *> V = DOMElement::getElementsByTagName(parent,tag,attrkey,attrvalue,trans);

		if ( !V.size() )
		{
			libmaus2::autoarray::AutoArray< ::XMLCh > const A = trans.transcodeNullTerminate(tag.c_str());
			xercesc::DOMElement * xnewel = doc->createElement(A.begin());
			DOMElement newel(xnewel);
			newel.setAttribute(attrkey,attrvalue,trans);
			newel.append(parent);
			V = DOMElement::getElementsByTagName(parent,tag,attrkey,attrvalue,trans);
		}

		assert ( V.size() );
		assert ( getTagName(V[0],trans) == tag );
		assert ( hasAttribute(V[0],attrkey,trans) == true );
		assert ( getAttribute(V[0],attrkey,trans) == attrvalue );

		return V[0];
	}

};

template<typename type>
void DOMElement::setAttributeTemplate(std::string const & key, type const & value, XMLTranscoder & trans)
{
	std::ostringstream ostr;
	ostr << value;
	setAttribute(key,ostr.str(),trans);
}
#endif
