/**
    bwtb3m
    Copyright (C) 2020 German Tischler-Höhle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
#if ! defined(DOMLSSERIALISER_HPP)
#define DOMLSSERIALISER_HPP

#include <xercesc/dom/DOMImplementation.hpp>
#include <xercesc/framework/MemBufFormatTarget.hpp>
#include <xercesc/dom/DOMLSOutput.hpp>
#include <xercesc/dom/DOMLSSerializer.hpp>
#include <string>
#include <DOMDocument.hpp>

struct DOMLSSerialiser
{
	xercesc::DOMLSSerializer * serialiser;

	DOMLSSerialiser(xercesc::DOMLSSerializer * rserialiser);
	~DOMLSSerialiser();
	std::string serialiseToString(DOMDocument & document, xercesc::DOMImplementation * impl);
	std::string serialiseToString(xercesc::DOMNode * document, xercesc::DOMImplementation * impl);
};
#endif
