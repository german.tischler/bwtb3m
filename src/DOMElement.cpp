/**
    bwtb3m
    Copyright (C) 2020 German Tischler-Höhle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
#include <DOMElement.hpp>

DOMElement::DOMElement(xercesc::DOMElement * rel)
: el(rel)
{

}
DOMElement::~DOMElement()
{
	if ( el )
	{
		// delete el;
	}
}

void DOMElement::append(xercesc::DOMElement * parent)
{
	parent->appendChild(el);
	el = nullptr;
}


void DOMElement::setAttribute(std::string const & key, std::string const & value, XMLTranscoder & trans)
{
	setAttribute(el,key,value,trans);
}

void DOMElement::setAttribute(xercesc::DOMElement * el, std::string const & key, std::string const & value, XMLTranscoder & trans)
{
	libmaus2::autoarray::AutoArray< ::XMLCh > xkey = trans.transcodeNullTerminate(key);
	libmaus2::autoarray::AutoArray< ::XMLCh > xvalue = trans.transcodeNullTerminate(value);
	el->setAttribute(xkey.begin(),xvalue.begin());
}

//
std::string DOMElement::getTagName(xercesc::DOMElement const * el, XMLTranscoder & trans)
{
	return trans.transcode(el->getTagName());
}
std::string DOMElement::getTagName(XMLTranscoder & trans) const
{
	return getTagName(el,trans);
}
std::vector<xercesc::DOMElement *> DOMElement::getElementsByTagName(xercesc::DOMElement * el, std::string const & tagname, XMLTranscoder & trans)
{
	std::vector<xercesc::DOMElement *> V;
	for (
		xercesc::DOMElement * p = el->getFirstElementChild();
		p ;
		p = p->getNextElementSibling()
	)
		if ( getTagName(p,trans) == tagname )
		{
			V.push_back(p);
		}

	return V;
}
std::vector<xercesc::DOMElement *> DOMElement::getChildElements(xercesc::DOMElement * el)
{
	std::vector<xercesc::DOMElement *> V;
	for (
		xercesc::DOMElement * p = el->getFirstElementChild();
		p ;
		p = p->getNextElementSibling()
	)
		V.push_back(p);

	return V;
}

bool DOMElement::hasAttribute(xercesc::DOMElement * el, std::string const & attrname, XMLTranscoder & trans)
{
	libmaus2::autoarray::AutoArray< ::XMLCh > xattrname(trans.transcodeNullTerminate(attrname));
	::xercesc::DOMAttr const * x = el->getAttributeNode(xattrname.begin());
	return x != nullptr;
}

std::string DOMElement::getAttribute(xercesc::DOMElement * el, std::string const & attrname, XMLTranscoder & trans)
{
	libmaus2::autoarray::AutoArray< ::XMLCh > xattrname(trans.transcodeNullTerminate(attrname));
	::XMLCh const * x = el->getAttribute(xattrname.begin());

	if ( x == nullptr )
	{
		std::cerr << "[W] attribute " << attrname << " does not exist in " << getTagName(el,trans) << std::endl;
		return std::string();
	}
	else
	{
		return trans.transcode(x);
	}
}

std::string DOMElement::getText(xercesc::DOMElement * el, XMLTranscoder & trans)
{
	std::ostringstream ostr;
	for ( xercesc::DOMNode * p = el->getFirstChild(); p; p = p->getNextSibling() )
		if ( p->getNodeType() == xercesc::DOMNode::TEXT_NODE )
		{
			xercesc::DOMText * t = reinterpret_cast<xercesc::DOMText *>(p);
			ostr << trans.transcode(t->getNodeValue());
		}

	return ostr.str();
}

std::string DOMElement::getTextRec(xercesc::DOMElement * el, XMLTranscoder & trans)
{
	std::ostringstream ostr;
	for ( xercesc::DOMNode * p = el->getFirstChild(); p; p = p->getNextSibling() )
		if ( p->getNodeType() == xercesc::DOMNode::TEXT_NODE )
		{
			xercesc::DOMText * t = reinterpret_cast<xercesc::DOMText *>(p);
			ostr << trans.transcode(t->getNodeValue());
		}
		else if ( p->getNodeType() == xercesc::DOMNode::ELEMENT_NODE )
		{
			xercesc::DOMElement * t = reinterpret_cast<xercesc::DOMElement *>(p);
			ostr << getTextRec(t,trans);
		}

	return ostr.str();
}

std::vector<xercesc::DOMElement *> DOMElement::getElementsByTagName(
	xercesc::DOMElement * el, std::string const & tagname,
	std::string const & attrname, std::string const & attrvalue,
	XMLTranscoder & trans
)
{
	std::vector<xercesc::DOMElement *> V = getElementsByTagName(el,tagname,trans);

	uint64_t j = 0;
	for ( uint64_t i = 0; i < V.size(); ++i )
		if (
			hasAttribute(V[i],attrname,trans)
			&&
			getAttribute(V[i],attrname,trans) == attrvalue
		)
			V[j++] = V[i];

	V.resize(j);

	return V;
}

std::vector<xercesc::DOMElement *> DOMElement::getElementsByTagNameCascade(
	xercesc::DOMElement * el, std::vector<std::string> const & Vtagname, XMLTranscoder & trans)
{
	std::vector<xercesc::DOMElement *> V(1,el);

	for ( uint64_t i = 0; i < Vtagname.size(); ++i )
	{
		std::vector<xercesc::DOMElement *> NV;

		for ( uint64_t j = 0; j < V.size(); ++j )
		{
			std::string const tagsym = Vtagname[i];

			std::string tagname = tagsym;
			std::string argkey;
			std::string argvalue;
			bool havearg = false;

			if (
				tagname.size() && tagname[tagname.size()-1] == ']'
				&&
				tagname.find("[") != std::string::npos
			)
			{
				std::string::size_type const p = tagname.find_last_of('[');
				assert ( p != std::string::npos );
				std::string arg = tagname.substr(p+1);
				arg = arg.substr(0,arg.size()-1);

				if ( arg.find('=') != std::string::npos )
				{
					std::string::size_type const ap = arg.find('=');
					argkey = arg.substr(0,ap);
					argvalue = arg.substr(ap+1);
					tagname = tagname.substr(0,p);
					havearg = true;

					// std::cerr << "tagname=" << tagname << " argkey=" << argkey << " argvalue=" << argvalue << std::endl;
				}
			}

			std::vector<xercesc::DOMElement *> LV = getElementsByTagName(V[j],tagname,trans);
			for ( uint64_t k = 0; k < LV.size(); ++k )
			{
				bool keep = true;

				if ( havearg )
				{
					keep = hasAttribute(LV[k],argkey,trans)
						&&
						getAttribute(LV[k],argkey,trans) == argvalue;

					// std::cerr << "keep=" << keep << std::endl;
				}

				if ( keep )
					NV.push_back(LV[k]);
			}
		}

		NV.swap(V);
	}

	return V;
}
