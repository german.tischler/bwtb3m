/**
    bwtb3m
    Copyright (C) 2020 German Tischler-Höhle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
#include <XMLTranscoder.hpp>

XMLTranscoder::XMLTranscoder(xercesc::XMLTranscoder * rtrans)
: trans(rtrans)
{
}

XMLTranscoder::~XMLTranscoder()
{
	delete trans;
}

libmaus2::autoarray::AutoArray< ::XMLCh > XMLTranscoder::nullTerminate(libmaus2::autoarray::AutoArray< ::XMLCh > A)
{
	uint64_t const s = A.size();
	A.resize(s+1);
	A[s] = 0;
	return A;
}

libmaus2::autoarray::AutoArray< ::XMLCh > XMLTranscoder::transcode(std::string const & s)
{
	uint64_t outsize = s.size();

	libmaus2::autoarray::AutoArray< ::XMLByte > I(s.size());
	std::copy(s.begin(),s.end(),I.begin());

	while ( true )
	{
		::XMLSize_t bytesEaten = 0;
		libmaus2::autoarray::AutoArray< ::XMLCh > A(outsize);
		libmaus2::autoarray::AutoArray<unsigned char> charSizes(outsize);
		::XMLSize_t const produced = trans->transcodeFrom(
			I.begin(),
			I.size(),
			A.begin(),
			A.size(),
			bytesEaten,
			charSizes.begin()
		);

		if ( bytesEaten >= I.size() )
		{
			assert ( bytesEaten == I.size() );
			A.resize(produced);
			return A;
		}
		else
		{
			outsize *= 2;
		}
	}
}

libmaus2::autoarray::AutoArray< ::XMLCh > XMLTranscoder::transcodeNullTerminate(std::string const & s)
{
	return nullTerminate(transcode(s));
}

std::string XMLTranscoder::transcode(::XMLCh const * x, uint64_t const s)
{
	uint64_t outsize = s;

	while ( true )
	{
		::XMLSize_t charsEaten = 0;
		TA.ensureSize(outsize);
		::XMLSize_t const outchars = trans->transcodeTo(x,s,TA.begin(),outsize,charsEaten,xercesc::XMLTranscoder::UnRep_RepChar);

		if ( charsEaten >= s )
		{
			assert ( charsEaten == s );
			return std::string(TA.begin(),TA.begin()+outchars);
		}
		else
		{
			outsize *= 2;
		}
	}
}

std::string XMLTranscoder::transcode(::XMLCh const * x)
{
	uint64_t s = 0;
	while ( *x )
	{
		++x;
		++s;
	}

	x -= s;

	return transcode(x,s);
}
