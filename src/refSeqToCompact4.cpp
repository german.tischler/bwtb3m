/**
    bwtb3m
    Copyright (C) 2020 German Tischler-Höhle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
#include <libmaus2/aio/InputStreamInstance.hpp>
#include <libmaus2/bitio/CompactArrayWriterFile.hpp>
#include <libmaus2/bitio/CompactDecoderBuffer.hpp>
#include <libmaus2/fastx/StreamFastAReader.hpp>
#include <libmaus2/lz/PlainOrGzipStream.hpp>
#include <libmaus2/random/Random.hpp>
#include <libmaus2/util/ArgParser.hpp>
#include <libmaus2/util/TabEntry.hpp>
#include <libmaus2/util/Concat.hpp>
#include <libmaus2/util/TempFileNameGenerator.hpp>
#include <libmaus2/parallel/NumCpus.hpp>
#include <libmaus2/fastx/FastALineParser.hpp>
#include <regex>

#include <XercesInit.hpp>

XercesInit xinit;

struct RegexReplace
{
	std::vector < std::pair < std::shared_ptr<std::regex>, std::string > > Vregex;

	RegexReplace(libmaus2::util::ArgParser const & arg)
	{
		if ( arg.argPresent("urlregex") )
		{
			std::vector<std::string> const V = arg("urlregex");

			for ( uint64_t i = 0; i < V.size(); ++i )
			{
				std::string const & s = V[i];
				std::string::size_type const p = s.find('|');

				if ( p == std::string::npos )
				{
					libmaus2::exception::LibMausException lme;
					lme.getStream() << "[E] unable to find separator | in " << s << std::endl;
					lme.finish();
					throw lme;
				}

				std::string const sreg = s.substr(0,p);
				std::string const srepl = s.substr(p+1);

				std::shared_ptr<std::regex> ptr(new std::regex(sreg));

				Vregex.push_back(std::make_pair(ptr,srepl));
			}
		}
	}

	std::string operator()(std::string s) const
	{
		for ( uint64_t i = 0; i < Vregex.size(); ++i )
			s = std::regex_replace(s,*(Vregex[i].first),Vregex[i].second,std::regex_constants::format_first_only);

		return s;
	}
};

static bool loadFile(std::string const & url, std::string & data, bool doclip)
{
	try
	{
		libmaus2::aio::InputStreamInstance ISI(url);
		std::ostringstream ostr;

		while ( ISI.peek() != std::istream::traits_type::eof() )
			ostr.put(ISI.get());

		data = ostr.str();

		if ( doclip )
		{
			uint64_t clip = 0;
			while ( clip < data.size() && isspace(data[data.size()-clip-1]) )
				++clip;

			data = data.substr(0,data.size()-clip);
		}

		return true;
	}
	catch(std::exception const & ex)
	{
		return false;
	}
}

struct ReverseStream
{
	libmaus2::util::TempFileNameGenerator & TFNG;
	uint64_t const buffersize;
	std::vector<std::string> Vfn;
	libmaus2::autoarray::AutoArray<char> A;
	char * ca;
	char * cc;
	char * ce;

	ReverseStream(libmaus2::util::TempFileNameGenerator & rTFNG, uint64_t const rbuffersize)
	: TFNG(rTFNG), buffersize(rbuffersize), Vfn(), A(buffersize,false), ca(A.begin()), cc(A.end()), ce(A.end())
	{
	}

	void flush()
	{
		if ( cc != ce )
		{
			std::string const fn = TFNG.getFileName(true /* register as temp */);
			Vfn.push_back(fn);
			libmaus2::bitio::CompactArrayWriterFile::unique_ptr_type pcompactout(new libmaus2::bitio::CompactArrayWriterFile(fn,2 /* bits per symbol */));
			pcompactout->COS->exceptions(std::istream::failbit | std::istream::badbit);
			pcompactout->write(cc,ce-cc);
			pcompactout->flush();
			pcompactout.reset();
			cc = ce;
		}
	}

	template<typename iterator>
	void put(iterator da, iterator de)
	{
		while ( da != de )
		{
			while ( da != de && cc != ca )
				*(--cc) = *(da++)^3;

			if ( cc == ca )
				flush();
		}
	}

	void concat(libmaus2::bitio::CompactArrayWriterFile & out)
	{
		flush();

		for ( uint64_t ii = 0; ii < Vfn.size(); ++ii )
		{
			uint64_t const i = Vfn.size()-ii-1;
			std::string const fn = Vfn[i];

			{
				libmaus2::bitio::CompactDecoderWrapper dec(fn);
				dec.exceptions(std::ostream::badbit);

				while ( dec )
				{
					dec.read(A.begin(),A.size());
					uint64_t const s = dec.gcount();
					out.write(A.begin(),s);
				}
			}

			libmaus2::aio::FileRemoval::removeFile(fn);
		}
	}
};

static uint64_t getDefaultNumThreads() { return libmaus2::parallel::NumCpus::getNumLogicalProcessors(); }

void refSeqToCompact4(libmaus2::util::ArgParser const & arg)
{
	std::string const url = arg.uniqueArgPresent("url") ? arg["url"] : std::string("ftp://ftp.ncbi.nlm.nih.gov/genomes/ASSEMBLY_REPORTS/assembly_summary_refseq.txt");
	std::string const tmpfilebase = arg.uniqueArgPresent("T") ? arg["T"] : libmaus2::util::ArgInfo::getDefaultTmpFileName(arg.progname);
	std::string const outputprefix = arg.uniqueArgPresent("outputprefix") ? arg["outputprefix"] : std::string("refSeqToCompact4_output");
	RegexReplace const regrepl(arg);
	bool const norc = arg.uniqueArgPresent("norc");
	std::regex headerregex("^#\\s*");
	unsigned int threads = arg.uniqueArgPresent("threads") ? arg.getUnsignedNumericArg<uint64_t>("threads") : 1;
	if ( threads == 0 )
		threads = getDefaultNumThreads();

	// std::cerr << "url " << regrepl(url) << std::endl;

	std::string listdata;
	bool listdataok = loadFile(regrepl(url),listdata,false /* doclip */);
	if ( ! listdataok )
	{
		libmaus2::exception::LibMausException lme;
		lme.getStream() << "[E] unable to load file " << regrepl(url) << std::endl;
		lme.finish();
		throw lme;
	}
	std::cerr << "[V] obtained file " << url << " of size " << listdata.size() << std::endl;

	typedef libmaus2::util::TabEntry<'\t','\001' /* avoid end of line inside quoted string */> tab_entry_type;

	std::map<std::string,uint64_t> Mcol;
	uint64_t ftp_path_col = 0;
	bool haveheader = false;

	{
		std::istringstream sumISI(listdata);
		std::string line;
		tab_entry_type TE;

		while ( std::getline(sumISI,line) && line.size() && line[0] == '#' )
		{
			line = std::regex_replace(line,headerregex,std::string());

			char const * ca = line.c_str();
			char const * ce = ca + line.size();
			TE.parse(ca,ca,ce);

			bool haveaccessioncol = false;
			for ( uint64_t i = 0; i < TE.size(); ++i )
			{
				std::string const col = TE.getString(i,ca);
				haveaccessioncol = haveaccessioncol || (col == "assembly_accession");
			}

			if ( haveaccessioncol )
			{
				if ( haveheader )
				{
					libmaus2::exception::LibMausException lme;
					lme.getStream() << "[E] file does not have unique header line (multiple header lines with assembly_accession)" << std::endl;
					lme.finish();
					throw lme;
				}

				for ( uint64_t i = 0; i < TE.size(); ++i )
				{
					std::string const col = TE.getString(i,ca);

					if ( Mcol.find(col) == Mcol.end() )
					{
						Mcol[col] = i;
					}
					else
					{
						libmaus2::exception::LibMausException lme;
						lme.getStream() << "[E] column " << col << " appears multiple times in " << line << std::endl;
						lme.finish();
						throw lme;
					}
				}

				if ( Mcol.find("ftp_path") == Mcol.end() )
				{
					libmaus2::exception::LibMausException lme;
					lme.getStream() << "[E] column " << "ftp_path" << " does not appear in " << line << std::endl;
					lme.finish();
					throw lme;
				}
				else
				{
					ftp_path_col = Mcol.find("ftp_path")->second;
				}

				haveheader = true;
			}
		}
	}

	if ( ! haveheader )
	{
		libmaus2::exception::LibMausException lme;
		lme.getStream() << "[E] header was not found in " << url << std::endl;
		lme.finish();
		throw lme;
	}

	if ( !listdata.size() || listdata[listdata.size()-1] != '\n' )
	{
		libmaus2::exception::LibMausException lme;
		lme.getStream() << "[E] file does not end with new line " << url << std::endl;
		lme.finish();
		throw lme;
	}

	std::vector<uint64_t> Vlinestart;
	Vlinestart.push_back(0);
	for ( uint64_t i = 1; i < listdata.size(); ++i )
		if ( listdata[i-1] == '\n' )
			Vlinestart.push_back(i);
	uint64_t numlines = Vlinestart.size();

	// numlines = std::min(numlines,static_cast<uint64_t>(8));

	Vlinestart.push_back(listdata.size());

	uint64_t const linesperthread = numlines ? ((numlines + threads - 1) / threads) : 0;
	uint64_t const numpacks = numlines ? ((numlines + linesperthread - 1) / linesperthread) : 0;

	struct ThreadContext
	{
		std::string const fnbase;
		libmaus2::util::TempFileNameGenerator TFNG;
		ReverseStream RS;
		std::string const compactfn;
		std::string const xmlfn;
		libmaus2::bitio::CompactArrayWriterFile::unique_ptr_type pcompactout;
		libmaus2::aio::OutputStreamInstance::unique_ptr_type pxmlout;

		static std::string computeFnBase(
			std::string const & tmpfilebase,
			uint64_t const id
		)
		{
			std::ostringstream fnostr;
			fnostr << tmpfilebase << "_" << id << "_";
			return fnostr.str();
		}

		ThreadContext(
			std::string const & tmpfilebase,
			uint64_t const id,
			bool const norc
		) : fnbase(computeFnBase(tmpfilebase,id)), TFNG(fnbase+"rev",5 /* depth */), RS(TFNG,norc?0:(32*1024*1024)), compactfn(fnbase + "compact"), xmlfn(fnbase + "xml"),
		    pcompactout(new libmaus2::bitio::CompactArrayWriterFile(compactfn,2 /* bits per symbol */)),
		    pxmlout(new libmaus2::aio::OutputStreamInstance(xmlfn))
		{
		}

		void concat(libmaus2::bitio::CompactArrayWriterFile & out)
		{
			{
				libmaus2::bitio::CompactDecoderWrapper dec(compactfn);
				dec.exceptions(std::ostream::badbit);
				libmaus2::autoarray::AutoArray<char> A(16*1024,false);

				while ( dec )
				{
					dec.read(A.begin(),A.size());
					uint64_t const s = dec.gcount();
					out.write(A.begin(),s);
				}

			}

			libmaus2::aio::FileRemoval::removeFile(compactfn);
		}


	};

	libmaus2::autoarray::AutoArray< std::unique_ptr<ThreadContext> > ATC(numpacks);
	for ( uint64_t t = 0; t < numpacks; ++t )
	{
		std::unique_ptr<ThreadContext> ptr(new ThreadContext(tmpfilebase,t,norc));
		ATC[t] = std::move(ptr);
	}

	std::regex slashend("/*$");
	std::regex toslash("^.*/");

	// rc mapping for mapped symbols
	libmaus2::autoarray::AutoArray<uint8_t> ctable(256,false);
	std::fill(ctable.begin(),ctable.end(),4);
	ctable[0] = 3; // A->T
	ctable[1] = 2; // C->G
	ctable[2] = 1; // G->C
	ctable[3] = 0; // T->A

	std::unique_ptr<XMLTranscoder> ptrans(xinit.getTranscoder());
	xercesc::DOMImplementation * impl = xinit.getImplementation();

	std::atomic<uint64_t> linesfinished(0);
	std::atomic<int> parfailed(0);
	std::atomic<uint64_t> gseqlen(0);
	std::atomic<uint64_t> anumassemblies(0);
	std::atomic<uint64_t> anumsequences(0);

	#if defined(_OPENMP)
	#pragma omp parallel for num_threads(threads)
	#endif
	for ( uint64_t t = 0; t < numpacks; ++t )
	{
		try
		{
			uint64_t const tlow = t * linesperthread;
			uint64_t const thigh = std::min(tlow+linesperthread,numlines);
			tab_entry_type TE;
			ThreadContext & TC = *(ATC[t]);
			ReverseStream & RS = TC.RS;
			uint64_t tseqlen = 0;
			libmaus2::autoarray::AutoArray<char> ASID;

			// uint64_t op = 0;
			libmaus2::bitio::CompactArrayWriterFile::unique_ptr_type & pcompactout = TC.pcompactout;
			libmaus2::aio::OutputStreamInstance::unique_ptr_type & pXML = TC.pxmlout;
			std::unique_ptr<DOMLSSerialiser> user(XercesInit::getSerialiser(impl));

			// (*pXML) << "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\" ?>\n";
			// (*pXML) << "<refSeqToCompact4>\n";

			for ( uint64_t lineid = tlow; lineid < thigh; ++lineid )
			{
				char const * ca = listdata.c_str() + Vlinestart[lineid+0];
				char const * ce = listdata.c_str() + Vlinestart[lineid+1] - 1;
				std::string const line(ca,ce);
				TE.parse(ca,ca,ce);

				if ( ftp_path_col < TE.size() )
				{
					std::string ftp_path = TE.getString(ftp_path_col,ca);
					ftp_path = std::regex_replace(ftp_path,slashend,std::string());

					std::string base = std::regex_replace(ftp_path,toslash,std::string());

					// assembly_status.txt
					std::string const prefix = regrepl(ftp_path);
					std::string const statusfn = prefix + "/assembly_status.txt";
					std::string statusdata;

					bool const statusok = loadFile(statusfn,statusdata,true); /* do clip */

					if ( statusok && statusdata == "status=latest" )
					{
						std::string const genomicfn = prefix + "/" + base + "_genomic.fna.gz";

						bool streamok = false;
						libmaus2::aio::InputStreamInstance::unique_ptr_type pstream;

						try
						{
							libmaus2::aio::InputStreamInstance::unique_ptr_type tstream(new libmaus2::aio::InputStreamInstance(genomicfn));
							pstream = std::move(tstream);
							streamok = true;
						}
						catch(std::exception const & ex)
						{
							std::cerr << ex.what() << std::endl;
							streamok = false;
						}

						if ( streamok )
						{
							std::istream & rawistr = *pstream;
							rawistr.exceptions ( std::istream::failbit | std::istream::badbit );
							libmaus2::lz::PlainOrGzipStream POGS(rawistr);
							libmaus2::fastx::FastALineParser FALP(POGS);

							// #define DEBUG

							struct AssemblyDocument
							{
								XMLTranscoder & trans;
								xercesc::DOMImplementation * impl;
								std::unique_ptr<DOMDocument> udoc;

								AssemblyDocument(
									XMLTranscoder & rtrans,
									xercesc::DOMImplementation * rimpl,
									std::map<std::string,uint64_t> const & Mcol,
									tab_entry_type const & TE,
									char const * ca,
									uint64_t const lineid
								) : trans(rtrans), impl(rimpl), udoc(XercesInit::getDocument("assembly",impl,trans))
								{
									xercesc::DOMElement * xroot = udoc->getRoot();
									for ( std::map<std::string,uint64_t>::const_iterator it = Mcol.begin(); it != Mcol.end(); ++it )
									{
										uint64_t const col = it->second;
										if ( col < TE.size() )
										{
											std::string const coldata = TE.getString(col,ca);
											if ( coldata.size() && it->first.size() )
												DOMElement::setAttribute(xroot,it->first,coldata,trans);
										}
									}
									{
										std::ostringstream ostr;
										ostr << lineid;
										DOMElement::setAttribute(xroot,"lineid",ostr.str(),trans);
									}


								}

								std::string toString(DOMLSSerialiser & user)
								{
									return user.serialiseToString(udoc->getRoot(),impl);
								}

								void addSequence(
									libmaus2::autoarray::AutoArray<char> const & ASID,
									uint64_t const oASID,
									std::vector < std::pair<uint64_t,uint64_t> > const & Vrepl,
									uint64_t const lseqlen,
									uint64_t const lseqid
									#if defined(DEBUG)
									, char const * ASEQ,
									uint64_t const oASEQ
									#endif
								)
								{
									std::unique_ptr<DOMElement> subel(udoc->createElement("sequence",trans));
									subel->setAttribute("id",std::string(ASID.begin(),ASID.begin()+oASID),trans);
									subel->setAttributeTemplate<uint64_t>("lseqid",lseqid,trans);
									subel->setAttributeTemplate<uint64_t>("length",lseqlen,trans);

									for ( uint64_t i = 1; i < Vrepl.size(); ++i )
									{
										assert (
											Vrepl[i-1].second < Vrepl[i].first
										);
									}

									for ( uint64_t i = 0; i < Vrepl.size(); ++i )
									{
										assert ( Vrepl[i].first < Vrepl[i].second );

										std::unique_ptr<DOMElement> repel(udoc->createElement("replace",trans));
										repel->setAttributeTemplate<uint64_t>("from",Vrepl[i].first,trans);
										repel->setAttributeTemplate<uint64_t>("to",Vrepl[i].second,trans);
										repel->append(subel->el);
									}

									#if defined(DEBUG)
									std::vector<std::pair<uint64_t,uint64_t> > Drepl;
									uint64_t low = 0;
									while ( low < oASEQ )
									{
										while ( low < oASEQ && libmaus2::fastx::mapChar(ASEQ[low]) < 4 )
											++low;

										if ( low < oASEQ )
										{
											uint64_t high = low+1;
											while ( high < oASEQ && libmaus2::fastx::mapChar(ASEQ[high]) >= 4 )
												++high;

											Drepl.push_back(std::make_pair(low,high));

											low = high;
										}
									}
									assert ( Vrepl == Drepl );
									#endif

									subel->append(udoc->getRoot());
								}
							};

							AssemblyDocument adoc(*ptrans,impl,Mcol,TE,ca,lineid);

							uint64_t oASID = 0;
							bool ASIDvalid = false;
							uint64_t lseqlen = 0;
							uint64_t lseqid = 0;
							std::pair<uint64_t,uint64_t> Prepl(0,0);
							std::vector < std::pair<uint64_t,uint64_t> > Vrepl;
							::libmaus2::fastx::FastALineParserLineInfo fainfo;


							#if defined(DEBUG)
							libmaus2::autoarray::AutoArray<char> ASEQ;
							uint64_t oASEQ = 0;
							#endif

							while ( FALP.getNextLine(fainfo) )
							{
								if ( fainfo.linetype == ::libmaus2::fastx::FastALineParserLineInfo::libmaus2_fastx_fasta_id_line )
								{
									if ( ASIDvalid )
									{
										if ( Prepl.first != Prepl.second )
											Vrepl.push_back(Prepl);

										adoc.addSequence(ASID,oASID,Vrepl,lseqlen,lseqid
											#if defined(DEBUG)
											,ASEQ.begin(),oASEQ
											#endif
										);
										tseqlen += lseqlen;
										++anumsequences;

										lseqid += 1;
									}

									oASID = 0;
									ASID.push(oASID,fainfo.line,fainfo.line+fainfo.linelen);
									ASIDvalid = true;
									Prepl = std::make_pair(0,0);
									Vrepl.resize(0);
									lseqlen = 0;

									#if defined(DEBUG)
									oASEQ = 0;
									#endif
								}
								else if ( fainfo.linetype == ::libmaus2::fastx::FastALineParserLineInfo::libmaus2_fastx_fasta_base_line )
								{
									#if defined(DEBUG)
									ASEQ.push(oASEQ,fainfo.line,fainfo.line + fainfo.linelen);
									#endif

									for ( uint64_t i = 0; i < fainfo.linelen; ++i )
									{
										int const sym = libmaus2::fastx::mapChar(fainfo.line[i]);

										if ( sym < 4 )
										{
											fainfo.line[i] = sym;
										}
										else
										{
											fainfo.line[i] = (libmaus2::random::Random::rand8() % 4);

											if ( Prepl.first == Prepl.second || Prepl.second != lseqlen+i )
											{
												if ( Prepl.first != Prepl.second )
													Vrepl.push_back(Prepl);

												Prepl.first = lseqlen+i;
												Prepl.second = Prepl.first+1;
											}
											else
											{
												Prepl.second += 1;
											}
										}
									}

									for ( uint64_t i = 0; i < fainfo.linelen; ++i )
										assert ( fainfo.line[i] < 4 );

									pcompactout->write(fainfo.line,fainfo.linelen);
									if ( ! norc )
										RS.put(fainfo.line,fainfo.line + fainfo.linelen);

									lseqlen += fainfo.linelen;
								}
							}

							if ( ASIDvalid )
							{
								if ( Prepl.first != Prepl.second )
									Vrepl.push_back(Prepl);

								adoc.addSequence(ASID,oASID,Vrepl,lseqlen,lseqid
									#if defined(DEBUG)
									,ASEQ.begin(),oASEQ
									#endif
								);
								tseqlen += lseqlen;
								++anumsequences;

								lseqid += 1;
							}

							std::string const xmldata = adoc.toString(*user);

							++anumassemblies;

							(*pXML) << xmldata;
						}
					}
				}

				uint64_t const llinesfinished = ++linesfinished;

				if ( llinesfinished % 32 == 0 )
				{
					libmaus2::aio::StreamLock::lock_type::scope_lock_type slock(libmaus2::aio::StreamLock::cerrlock);
					std::cerr << "[V] " << llinesfinished << "/" << numlines << std::endl;
				}
			}

			// (*pXML) << "</refSeqToCompact4>\n";
			pXML->flush();
			pXML.reset();

			pcompactout->flush();
			pcompactout.reset();

			gseqlen += tseqlen;
		}
		catch(std::exception const & ex)
		{
			parfailed = 1;
			libmaus2::aio::StreamLock::lock_type::scope_lock_type slock(libmaus2::aio::StreamLock::cerrlock);
			std::cerr << ex.what() << std::endl;
		}
	}

	if ( parfailed.load() )
	{
		libmaus2::exception::LibMausException lme;
		lme.getStream() << "[E] parallel loop failed while processing " << url << std::endl;
		lme.finish();
		throw lme;
	}

	uint64_t const totalseqlen = gseqlen.load();

	std::cerr << "[V] total sequence length (forward) is " << totalseqlen << std::endl;

	std::string const compactfn = outputprefix + ".compact";
	std::string const xmlfn = outputprefix + ".compact.xml";
	libmaus2::bitio::CompactArrayWriterFile::unique_ptr_type pcompactout(new libmaus2::bitio::CompactArrayWriterFile(compactfn,2 /* bits per symbol */));
	pcompactout->COS->exceptions(std::ostream::badbit);

	libmaus2::aio::OutputStreamInstance::unique_ptr_type pXML(new libmaus2::aio::OutputStreamInstance(xmlfn));
	(*pXML) << "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\" ?>\n";
	(*pXML) << "<refSeqToCompact4>\n";

	{
		std::unique_ptr<DOMDocument> udoc(XercesInit::getDocument("meta",impl,*ptrans));
		std::unique_ptr<DOMLSSerialiser> user(XercesInit::getSerialiser(impl));
		xercesc::DOMElement * xroot = udoc->getRoot();
		{
			std::ostringstream ostr;
			ostr << totalseqlen;
			DOMElement::setAttribute(xroot,"totalsequencelength",ostr.str(),*ptrans);
		}
		{
			std::ostringstream ostr;
			ostr << anumassemblies.load();
			DOMElement::setAttribute(xroot,"numassemblies",ostr.str(),*ptrans);
		}
		{
			std::ostringstream ostr;
			ostr << anumsequences.load();
			DOMElement::setAttribute(xroot,"numsequences",ostr.str(),*ptrans);
		}
		{
			std::ostringstream ostr;
			ostr << (norc?1:0);
			DOMElement::setAttribute(xroot,"norc",ostr.str(),*ptrans);
		}

		(*pXML) << user->serialiseToString(xroot,impl);
	}

	for ( uint64_t t = 0; t < ATC.size(); ++t )
	{
		ATC[t]->concat(*pcompactout);
	}
	for ( uint64_t t = 0; t < ATC.size(); ++t )
	{
		libmaus2::util::Concat::concat(ATC[t]->xmlfn,*pXML);
		libmaus2::aio::FileRemoval::removeFile(ATC[t]->xmlfn);
	}
	for ( uint64_t tt = 0; tt < ATC.size(); ++tt )
	{
		uint64_t const t = ATC.size()-tt-1;
		ATC[t]->RS.concat(*pcompactout);
	}

	(*pXML) << "</refSeqToCompact4>\n";
	pXML->flush();
	pXML.reset();

	pcompactout->flush();
	pcompactout.reset();

	for ( uint64_t t = 0; t < ATC.size(); ++t )
		ATC[t].reset();

	uint64_t const fs = libmaus2::bitio::CompactDecoderWrapper::getFileSize(compactfn);
	assert (
		(norc && fs == totalseqlen)
		||
		(!norc && fs == 2*totalseqlen)
	);

	if ( ! norc )
	{
		assert ( fs % 2 == 0 );
		uint64_t const n = fs/2;

		std::cerr << "[V] verifying output rc...";
		libmaus2::bitio::CompactDecoderWrapper CDW(compactfn);
		uint64_t const bs = 1024*1024;
		libmaus2::autoarray::AutoArray<char> A(bs,false);
		libmaus2::autoarray::AutoArray<char> B(bs,false);
		uint64_t const numblocks = (n + bs - 1) / bs;

		for ( uint64_t i = 0; i < numblocks; ++i )
		{
			uint64_t const low = i * bs;
			uint64_t const high = std::min(low+bs,n);
			uint64_t const rlow = 2*n-high;
			uint64_t const rhigh = 2*n-low;
			assert ( high-low == rhigh-rlow );

			CDW.clear();
			CDW.seekg(low);
			CDW.read(A.begin(),high-low);
			assert ( CDW.gcount() == static_cast<int64_t>(high-low) );

			CDW.clear();
			CDW.seekg(rlow);
			CDW.read(B.begin(),rhigh-rlow);
			assert ( CDW.gcount() == static_cast<int64_t>(rhigh-rlow) );

			char const * a = A.begin();
			char const * b = B.begin()+(rhigh-rlow);

			for ( uint64_t j = 0; j < high-low; ++j )
			{
				char const ca = *(a++);
				char const cb = *(--b);
				assert ( ca == (cb ^ 3) );
			}
		}
		std::cerr << "done." << std::endl;
	}
}

int main(int argc, char * argv[])
{
	try
	{
		std::vector<libmaus2::util::ArgParser::ArgumentDefinition> Vformat;
		Vformat.push_back(libmaus2::util::ArgParser::ArgumentDefinition("u","url",true));
		Vformat.push_back(libmaus2::util::ArgParser::ArgumentDefinition("T","",true));
		Vformat.push_back(libmaus2::util::ArgParser::ArgumentDefinition("t","threads",true));
		Vformat.push_back(libmaus2::util::ArgParser::ArgumentDefinition("o","outputprefix",true));
		Vformat.push_back(libmaus2::util::ArgParser::ArgumentDefinition("R","urlregex",true));
		Vformat.push_back(libmaus2::util::ArgParser::ArgumentDefinition("","norc",false));
		libmaus2::util::ArgParser const arg(argc,argv,Vformat);
		refSeqToCompact4(arg);
	}
	catch(std::exception const & ex)
	{
		std::cerr << ex.what() << std::endl;
	}
}
